package ru.saveidea.ormtree.example.model {
	import ru.saveidea.tree.models.TreeNodeType;

	import mx.collections.ArrayCollection;

	/**
	 * @author antonsidorenko
	 */
	[Bindable]
	public class CourseModel extends ORMTreeModel {
		
		[Mapped]
		public var title : String = "Course Default Title";
		
		[Mapped]
		public var value : Number;
		
		private var _scenes : ArrayCollection = new ArrayCollection();

		[OneToMany(indexed='true', lazy='true', isHidden='true')]
		public function set scenes(value : ArrayCollection) : void {
			_scenes = value;
		}

		public function get scenes() : ArrayCollection {
			return _scenes;
		}
		
		private var _tests : ArrayCollection = new ArrayCollection();

		[OneToMany(indexed='true', lazy='true', isHidden='true')]
		public function set tests(value : ArrayCollection) : void {
			_tests = value;
		}

		public function get tests() : ArrayCollection {
			return _tests;
		}
		
		// Actions
		protected var _actions : ArrayCollection = new ArrayCollection(['one', 'two','thre']);

		[Transient]
		public function set actions(value : ArrayCollection) : void {
			_actions = value;
		}

		public function get actions() : ArrayCollection {
			return _actions;
		}
		
		[Mapped(orderIndex="2", dataProvider="actions")]
		public var action : String;

		override public function allowedChildTypesForList(propertyName : String) : Vector.<TreeNodeType> {
			if (propertyName == "scenes") {
				return new <TreeNodeType>[new TreeNodeType(SceneModel, "Scene")];
			}

			return null;
		}

		override public function toString() : String {
			return title;
		}
	}
}