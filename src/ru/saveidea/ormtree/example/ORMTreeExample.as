package ru.saveidea.ormtree.example {
	import flash.filesystem.File;
	import flash.text.Font;
	
	import ru.saveidea.base.Document;
	import ru.saveidea.ormtree.adminpanel.ORMAdminPanelAir;
	import ru.saveidea.ormtree.adminpanel.ORMAdminPanelAirSettings;
	import ru.saveidea.ormtree.example.model.CourseModel;

	/**
	 * @author antonsidorenko
	 */
	public class ORMTreeExample extends Document {
		private var adminPanel : ORMAdminPanelAir;

		public function ORMTreeExample() {
			super();
			Font.registerFont(HelveticaNeue);
			/*
			var c : CourseModel = new CourseModel();
			c.title = "Course!!!";

			var m : SceneModel = makeSceneModel("Scene 0");
			var m2 : SceneModel = makeSceneModel("Scene 1");

			c.scenes.addItem(m);
			c.scenes.addItem(m2);

			var e : Element = new Element();

			m.elements.addItem(e);
			 * 
			 */

			var dbFile : File = File.documentsDirectory.resolvePath("ORMTreeExample/db");
			var filesLocation : File = File.documentsDirectory.resolvePath("ORMTreeExample/files");
			var settings : ORMAdminPanelAirSettings = new ORMAdminPanelAirSettings(dbFile, CourseModel, filesLocation);

			adminPanel = new ORMAdminPanelAir(settings);
			addChild(adminPanel);
		}
	}
}