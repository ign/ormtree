package ru.saveidea.ormtree.view {
	import caurina.transitions.Tweener;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.getTimer;
	
	import ru.saveidea.ormtree.manager.ORMTreeManager;
	import ru.saveidea.tree.view.TreeNodeSkinBase;
	import ru.saveidea.tree.view.TreeNodeViewBase;
	import ru.saveidea.tree.view.TreeNodeViewIcons;

	/**
	 * @author antonsidorenko
	 */
	public class ORMTreeNodeSkin extends TreeNodeSkinBase {
		
		protected var textfield : TextField;
		private var dtfNormal : TextFormat;
		private var dtfSelected : TextFormat;
		private var dtfOver:TextFormat;
		private var container : Sprite;
		private var folderIcon : MovieClip;
		private var lastClickTime : uint;
		private var nodeSkin:MovieClip;
		private var nodeSkinSelected:MovieClip;
		private static var shadowFilter:DropShadowFilter;
		private var oldPosY:Number=0;
		
		public function ORMTreeNodeSkin(view : TreeNodeViewBase) {
			super(view);

			if(!shadowFilter)
				shadowFilter=new DropShadowFilter(1,270,0,0.56,0,0,0.45);
			
			
			nodeSkin=	new (ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_SKIN) as Class)()
			addChild(nodeSkin);
			nodeSkin.width=1000;
			nodeSkin.x=-500; 
			
			nodeSkinSelected=new (ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_SELECTED_SKIN) as Class)()
			nodeSkinSelected.width=1000;
			nodeSkinSelected.x=-500;
			nodeSkinSelected.visible=false; 
			addChild(nodeSkinSelected);
			
			folderIcon = new (ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_FOLDER_ICON_SKIN) as Class)()
			folderIcon.gotoAndStop(1);
			folderIcon.x=int(folderIcon.width);
			folderIcon.y=int(nodeSkin.height*.5);

			textfield = new TextField();

			container = new Sprite();
			addChild(container);

			dtfOver=ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_LABEL_OVER_STATE_SKIN);
			dtfNormal = ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_LABEL_NORMAL_STATE_SKIN);
			dtfSelected= ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_LABEL_SLECTED_STATE_SKIN);
			textfield.defaultTextFormat  = dtfNormal; 

			textfield.autoSize = TextFieldAutoSize.LEFT;
			textfield.multiline = true;
			textfield.wordWrap = true;
			textfield.width = 300;
			textfield.x = folderIcon.x + 16 + 2;
			textfield.mouseEnabled = false;
			textfield.embedFonts=true;
				
			textfield.y=(nodeSkin.height-textfield.height)*.5-10;
			oldPosY=textfield.y;		
	
			view.childsContainer.x = textfield.x;
			view.childsContainer.y = nodeSkin.height;

			folderIcon.buttonMode = true;
			folderIcon.addEventListener(MouseEvent.CLICK, onIconClickHandler);
			this.addEventListener(MouseEvent.MOUSE_OVER,onSkinOver);
			this.addEventListener(MouseEvent.MOUSE_OUT,onSkinOut);
			//folderIcon.y = 20 / 2 - 16 / 2;

			addChild(folderIcon);
			addChild(textfield);

			addEventListener(MouseEvent.CLICK, onClickHandler);
		}
		
		protected function onSkinOut(event:MouseEvent):void
		{
			if (nodeSkinSelected.visible)
				return;
			textfield.setTextFormat(dtfNormal);
		}
		
		protected function onSkinOver(event:MouseEvent):void
		{
			if (nodeSkinSelected.visible)
				return;
			textfield.setTextFormat(dtfOver);
		}
		
		private function onDoubleClickHandler(event : MouseEvent) : void {
			if (view.isCollapsed) {
				view.expand();
			} else {
				view.collapse();
			}
			updateIconCollapseState();
		}

		private function getTitle() : String {
			if (data is ORMTreeNodeData) {
				return (data as ORMTreeNodeData).label;
			} else {
				return String(data);
			}
			
			return null;
		}

		private function checkTitlePosition():void
		{
			textfield.text = getTitle();
			if (textfield.numLines>1)
			{
				dtfNormal = ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_LABEL_NORMAL_STATE_SKIN_LONG_TEXT);
				dtfSelected= ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_LABEL_SLECTED_STATE_SKIN_LONG_TEXT);
				dtfOver= ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_LABEL_OVER_STATE_SKIN_LONG_TEXT);
				
				textfield.y=oldPosY-5;
			}else
			{
				dtfNormal = ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_LABEL_NORMAL_STATE_SKIN);
				dtfSelected= ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_LABEL_SLECTED_STATE_SKIN);
				dtfOver= ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_NODE_LABEL_OVER_STATE_SKIN);
				textfield.y=oldPosY;
			}
			
			if (nodeSkinSelected.visible) 
			{
				textfield.defaultTextFormat = dtfSelected;
				textfield.setTextFormat(dtfSelected);
			}else
			{
				textfield.defaultTextFormat = dtfNormal;
				textfield.setTextFormat(dtfNormal);
			}
		
		}
		override public function changeState(state : String) : void {
			super.changeState(state);
			checkTitlePosition();
			if (state == TreeNodeViewBase.STATE_SELECTED) {
				// полоску рисуем
				/*container.graphics.clear();
				container.graphics.beginFill(0x3875d7);
				container.graphics.drawRect(-100, 0, 500, textfield.height);
				container.graphics.endFill();*/
				nodeSkinSelected.visible=true;
				folderIcon.gotoAndStop(2);

				textfield.defaultTextFormat = dtfSelected;
				textfield.text = getTitle();
				textfield.setTextFormat(dtfSelected);
				textfield.filters=[shadowFilter];
			} else if (state == TreeNodeViewBase.STATE_NORMAL) {
				// полоску убираем
				//container.graphics.clear();
			
				folderIcon.gotoAndStop(1);
				nodeSkinSelected.visible=false;
				textfield.defaultTextFormat = dtfNormal;
				textfield.text = getTitle();
				textfield.setTextFormat(dtfNormal);
				textfield.filters=[]
				// textfield.transform.colorTransform.color = 0xffffff;
			}
			
			
		}

		private function onClickHandler(event : MouseEvent) : void {
			event.stopPropagation();
			if (getTimer() - lastClickTime < 180) {
				onDoubleClickHandler(null);
			}
			view.select();
			lastClickTime = getTimer();
		}

		private function onIconClickHandler(event : MouseEvent) : void {
			if (view.childsContainer.numChildren > 0) {
				if (view.isCollapsed) {
					view.expand();
				} else {
					view.collapse();
				}
			}
			updateIconCollapseState();
		}

		override public function update() : void {
			super.update();

			if (view.childsContainer.numChildren > 0) {
				folderIcon.visible = true;
			} else {
				folderIcon.visible = false;
			}

			textfield.text = getTitle();
			view.childsContainer.y = nodeSkin.height;
			checkTitlePosition();
			updateIconCollapseState();
		}

		private function updateIconCollapseState() : void {
			if (view.isCollapsed) {
				Tweener.addTween(folderIcon,{rotation:0,time:0.5})
				//folderIcon.gotoAndStop(1);
			} else {
				Tweener.addTween(folderIcon,{rotation:90,time:0.5})
				//folderIcon.gotoAndStop(2);
			}
		}

		override public function destroy() : void {
			super.destroy();
			
			folderIcon.removeEventListener(MouseEvent.CLICK, onIconClickHandler);
			removeEventListener(MouseEvent.CLICK, onClickHandler);
		}

	}
}