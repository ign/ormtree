package ru.saveidea.ormtree.view {
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	import mx.collections.ArrayCollection;
	
	import ru.saveidea.orm.AccessorProperties;
	import ru.saveidea.orm.utils.DescribeTypeUtils;
	import ru.saveidea.ormtree.example.model.ORMTreeModel;
	import ru.saveidea.tree.models.TreeNodeType;
	import ru.saveidea.tree.view.TreeNodeViewAnimation;
	import ru.saveidea.tree.view.TreeNodeViewBase;
	import ru.saveidea.tree.view.TreeNodeViewBaseCustomEvent;
	import ru.saveidea.tree.view.TreeNodeViewBaseEvent;

	/**
	 * @author antonsidorenko
	 */
	public class ORMTreeNodeView extends TreeNodeViewAnimation {
		
		private var _contextMenu : ContextMenu;
		private var _typeByContextMenuItem : Dictionary;
		private var hashForDeleteNode : Dictionary;
		private var isDynamicContent : Boolean = false;

		public function ORMTreeNodeView(parentNodeView : TreeNodeViewBase) {
			super(parentNodeView);
			if (!parentNodeView)
				x += 10;
			hashForDeleteNode = new Dictionary();
			
			//_contextMenu = new
			
			if (!parentNodeView) {
				//самый верхний элемент
				if (!_contextMenu) {
					//создаём
					_contextMenu = new ContextMenu();					
				}
				var exportContextMenuItem : ContextMenuItem = new ContextMenuItem("Export");
				exportContextMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, onExportClickHandler);
				_contextMenu.customItems.push(exportContextMenuItem);
				
				skin.contextMenu = _contextMenu;
			}
		}

		private function onExportClickHandler(event : ContextMenuEvent) : void {
			getRootView().dispatchEvent(new TreeNodeViewBaseCustomEvent(TreeNodeViewBaseCustomEvent.CUSTOM_EVENT, data, "export"));
		}

		override public function deleteNode(node : TreeNodeViewBase) : void {
			var dataSpecified : ORMTreeNodeData = data as ORMTreeNodeData;
			var dataChilds : Array = (dataSpecified.data[dataSpecified.property] as ArrayCollection).source;

			for (var i : uint = 0;i < dataChilds.length;++i) {
				if (dataChilds[i] === node.data) {
					dataChilds.splice(i, 1);
					break;
				}
			}
			super.deleteNode(node);
		}

		override public function update() : void {
			if (isDynamicContent) {
				var dataSpecified : ORMTreeNodeData = data as ORMTreeNodeData;
				var dataChilds : Array = (dataSpecified.data[dataSpecified.property] as ArrayCollection).source;

				if (dataChilds.length != childs.length) {
					// пересоздать детей
					clear();

					var childViewClass : Class = getChildNodeViewClass();
					var childView : TreeNodeViewBase;

					for (var i : int = 0; i < dataChilds.length; i++) {
						childView = new childViewClass(this);
						childView.data = dataChilds[i];
						addChildView(childView);

						var pOrmNode : ORMTreeNodeView = childView.parentNodeView as ORMTreeNodeView;
						if (pOrmNode.isDynamicContent) {
							if (!hashForDeleteNode[childView]) {
								attachDeleteContextMenuForNode(childView as ORMTreeNodeView);
							}
						}
					}
				}
			}

			super.update();
		}

		private function attachDeleteContextMenuForNode(curr_node : ORMTreeNodeView) : void 
		{
			
			var new_contextMenu:ContextMenu = new ContextMenu();
					
			var item : ContextMenuItem = new ContextMenuItem("Delete");
			item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, onContextMenuItemDelete);
			new_contextMenu.customItems.push(item);
			curr_node.contextMenu = new_contextMenu;
			hashForDeleteNode[item] = curr_node;
		}

		override protected function onSwapNodes(drag_index : uint, sel_index : uint) : void {
			super.onSwapNodes(drag_index, sel_index);
			var dataSpecified : ORMTreeNodeData = data as ORMTreeNodeData;
			var dataChilds : Array = (dataSpecified.data[dataSpecified.property] as ArrayCollection).source;

			var drag_data : Object = dataChilds[drag_index];
			dataChilds.splice(drag_index, 1);
			dataChilds.splice(sel_index, 0, drag_data);
		}

		override protected function onMouseForDragNode(event : MouseEvent) : void {
			if (!isDynamicContent)
				return;
			super.onMouseForDragNode(event);
		}

		private static var hash_for_test:Dictionary;
		override public function set data(data : Object) : void {
			super.data = data;
			
			var childViewClass : Class = getChildNodeViewClass();
			var childView : TreeNodeViewBase;
			var childData : ORMTreeNodeData;
			var dataSpecified : ORMTreeNodeData;

			var i : int = 0;
			if (!hash_for_test)
				hash_for_test=new Dictionary();

			if (data is ORMTreeNodeData) {
				isDynamicContent = true;
				makeForDragAndDropNodes();

				dataSpecified = data as ORMTreeNodeData;
				var childs : Array = (dataSpecified.data[dataSpecified.property] as ArrayCollection).source;

				for (i = 0; i < childs.length; i++) {
					childView = new childViewClass(this);

					addChildView(childView);

					childView.data = childs[i];
					attachDeleteContextMenuForNode(childView as ORMTreeNodeView);
				}

				var model : ORMTreeModel = dataSpecified.data as ORMTreeModel;
				if (model) {
					if (!_contextMenu) {
						_contextMenu = new ContextMenu();
					}
					_typeByContextMenuItem = new Dictionary();

					var types : Vector.<TreeNodeType> = model.allowedChildTypesForList(dataSpecified.property);
					if (types) {
						var item : ContextMenuItem;
						for (i = 0; i < types.length; i++) {
							item = new ContextMenuItem("Add " + types[i].name);
							item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, onContextMenuItemSelectHandler);
							_contextMenu.customItems.push(item);

							_typeByContextMenuItem[item] = types[i];
						}

						// Настроить контекстное меню для создания вложенных элементов
						skin.contextMenu = _contextMenu;
					}
				}
			} else {
				// не было указано поле, детей которого надо выводить
				var modelClass : Class = getDefinitionByName(getQualifiedClassName(data)) as Class;
				var listAccessors : XML = DescribeTypeUtils.getOrderedAccessorsByArgumentInMetaDataList(modelClass, ["OneToMany"], "orderIndex");

				var accessors : XMLList = listAccessors.descendants("accessor");
				var accessorProperties : AccessorProperties;
				var accessor : XML;

				for (i = 0; i < accessors.length(); i++) {
					accessor = accessors[i];

					accessorProperties = new AccessorProperties();
					accessorProperties.parse(accessor);

					childView = new childViewClass(this);

					childData = new ORMTreeNodeData();
					childData.data = data;
					childData.property = accessorProperties.name;
					childData.label = accessorProperties.label;
					childView.data = childData;

					addChildView(childView);

					// modelToView[model] = childView;
				}
			}

			update();
		}

		private function onContextMenuItemDelete(event : ContextMenuEvent) : void {
			var childView : ORMTreeNodeView = hashForDeleteNode[event.target];
			childView.parentNodeView.deleteNode(childView);
			delete hashForDeleteNode[event.target];
			dispatchEvent(new TreeNodeViewBaseEvent(TreeNodeViewBaseEvent.UPDATE_VIEW,this,null,true));
		}

		private function onContextMenuItemSelectHandler(event : ContextMenuEvent) : void {
			var newNodeDefinition : Class = (_typeByContextMenuItem[event.target] as TreeNodeType).definition;
			var newNode : ORMTreeModel = new newNodeDefinition();

			var dataSpecified : ORMTreeNodeData = data as ORMTreeNodeData;
			newNode.parentListName = dataSpecified.property;
			newNode.parent = dataSpecified.data as ORMTreeModel;

			dispatchEvent(new TreeNodeViewBaseEvent(TreeNodeViewBaseEvent.ADD_CHILD, this, newNode, true));
			
		}

		override protected function getSkinClass() : Class {
			return ORMTreeNodeSkin;
		}

		override protected function onTreeNodeViewSelectHandler(event : TreeNodeViewBaseEvent) : void {
			super.onTreeNodeViewSelectHandler(event);

			if (event.nodeView.data is ORMTreeNodeData) {
				event.stopImmediatePropagation();
			}
		}

		override public function destroy() : void {
			super.destroy();

			// if (hashForDeleteNode[this])
			var parentView : ORMTreeNodeView = parentNodeView as ORMTreeNodeView;

			if (isDynamicContent) {
				this.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseForDragNode);
				this.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseForDragNode);
				if (stage)
					stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseForDragNode);
			}
		}
	}
}