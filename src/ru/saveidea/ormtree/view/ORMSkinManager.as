package ru.saveidea.ormtree.view
{
	import flash.text.TextFormat;
	
	import ru.saveidea.tree.view.TreeNodeViewIcons;

	public class ORMSkinManager
	{
		private static var _instance:ORMSkinManager;
		
		public static const TREE_NODE_FOLDER_ICON_SKIN:uint=1;
		public static const TREE_NODE_LABEL_NORMAL_STATE_SKIN:uint=2;
		public static const TREE_NODE_LABEL_SLECTED_STATE_SKIN:uint=3;
		public static const TREE_NODE_LABEL_OVER_STATE_SKIN:uint=4;
		public static const TREE_BACKGROUND_SKIN:uint=5;
		public static const TREE_NODE_SKIN:uint=6;
		public static const TREE_NODE_SELECTED_SKIN:uint=7;
		
		public static const TREE_NODE_LABEL_SLECTED_STATE_SKIN_LONG_TEXT:uint=8;
		public static const TREE_NODE_LABEL_OVER_STATE_SKIN_LONG_TEXT:uint=9;
		public static const TREE_NODE_LABEL_NORMAL_STATE_SKIN_LONG_TEXT:uint=10;
		
		private var _skins_arr:Array;
		public function ORMSkinManager(ref_lock:LockManager)
		{
			if (!ref_lock) 
				throw new Error("ORMTreeNodeSkinManager is Singletone, use ORMTreeNodeSkinManager.instance");
			
			_skins_arr=new Array();
			_skins_arr[TREE_NODE_FOLDER_ICON_SKIN]=TreeNodeIcoSkin;
			_skins_arr[TREE_NODE_LABEL_NORMAL_STATE_SKIN]=new TextFormat("Helvetica Neue", 16, 0);
			_skins_arr[TREE_NODE_LABEL_SLECTED_STATE_SKIN]=new TextFormat("Helvetica Neue", 16, 0xFFFFFF);
			_skins_arr[TREE_NODE_LABEL_OVER_STATE_SKIN]=new TextFormat("Helvetica Neue", 16, 0xf56a59);
			_skins_arr[TREE_BACKGROUND_SKIN]=SkinTreeBack;
			_skins_arr[TREE_NODE_SKIN]=TreeNodeSkin;
			_skins_arr[TREE_NODE_SELECTED_SKIN]=TreeNodeSelectedSkin;
			
			_skins_arr[TREE_NODE_LABEL_SLECTED_STATE_SKIN_LONG_TEXT]=new TextFormat("Helvetica Neue", 12, 0xFFFFFF);
			_skins_arr[TREE_NODE_LABEL_OVER_STATE_SKIN_LONG_TEXT]=new TextFormat("Helvetica Neue", 12,0xf56a59 );
			_skins_arr[TREE_NODE_LABEL_NORMAL_STATE_SKIN_LONG_TEXT]=new TextFormat("Helvetica Neue", 12, 0);
			
			
		} 
		
		public static function get instance() : ORMSkinManager 
		{
			if (!_instance) {
				_instance = new ORMSkinManager(new LockManager());
			}
			
			return _instance;
		}
		
		public function getSkinById(skin_id:uint):*
		{
			return _skins_arr[skin_id];
		}
		
	}
}

class LockManager
{
	
}