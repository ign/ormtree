package ru.saveidea.ormtree.adminpanel {
	import ru.saveidea.ormtree.manager.ORMTreeManager;
	import ru.saveidea.view.ManualSizedSprite;

	import flash.events.Event;

	/**
	 * @author antonsidorenko
	 */
	public class ORMAdminPanelBase extends ManualSizedSprite {
		
		private var useFullScreen : Boolean = true;
		private var _data : Object;
		protected var header : Header;
		public var treeManager : ORMTreeManager;
		protected var settings : ORMAdminPanelBaseSettings;

		public function ORMAdminPanelBase(settings : ORMAdminPanelBaseSettings) {
			this.settings = settings;

			header = new Header(settings.title);
			addChild(header);

			treeManager = makeTreeManager();
			treeManager.y = 50;
			addChild(treeManager);

			addEventListener(Event.ADDED_TO_STAGE, onAddedToStageHandler);
		}

		protected function makeTreeManager() : ORMTreeManager {
			return null;
		}

		protected function loadDB(db : Object) : void {
			
		}

		protected function onDBLoadCompleteHandler(dbContent : String) : void {
		}

		private function onAddedToStageHandler(event : Event) : void {
			stage.addEventListener(Event.RESIZE, onStageResizeHandler);
			onStageResizeHandler(null);

			loadDB(settings.db);
		}

		private function onStageResizeHandler(event : Event) : void {
			if (useFullScreen) {
				header.width = stage.stageWidth;
				treeManager.width = stage.stageWidth;
				treeManager.height = stage.stageHeight - 50;
			} else {
				header.width = width || stage.stageWidth;
				treeManager.width = width || stage.stageWidth;
				treeManager.height = height - 50 || stage.stageHeight - 50;
			}
		}

		override public function set height(value : Number) : void {
			super.height = value;

			useFullScreen = false;
		}

		public function set data(data : Object) : void {
			_data = data;

			treeManager.data = data;
		}
	}
}