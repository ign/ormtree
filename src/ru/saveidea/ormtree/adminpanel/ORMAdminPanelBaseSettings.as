package ru.saveidea.ormtree.adminpanel {
	import ru.saveidea.ormtree.view.ORMTreeNodeView;
	
	/**
	 * @author antonsidorenko
	 */
	 
	public class ORMAdminPanelBaseSettings {
		
		public var db : Object;
		public var rootModelClass : Class;
		public var title : String;
		public var nodeViewClass : Class;		
		
		public function ORMAdminPanelBaseSettings(db : Object, rootModelClass : Class, nodeViewClass : Class=null, title : String="Admin Panel") {
			
			this.db = db;
			this.rootModelClass = rootModelClass;
			this.nodeViewClass = nodeViewClass;
			this.title = title;
			
			if (this.nodeViewClass==null) {
				this.nodeViewClass = ORMTreeNodeView;
			}
			
		}
		
	}
}