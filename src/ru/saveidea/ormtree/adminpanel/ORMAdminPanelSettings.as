package ru.saveidea.ormtree.adminpanel {

	/**
	 * @author antonsidorenko
	 */
	public class ORMAdminPanelSettings extends ORMAdminPanelBaseSettings {
		
		public function ORMAdminPanelSettings(db : String, rootModelClass : Class, nodeViewClass : Class = null, title : String = "Admin Panel") {
			super(db, rootModelClass, nodeViewClass, title);
		}
		
	}
}