package ru.saveidea.ormtree.adminpanel {
	import ru.saveidea.orm.forms.ModelForm;
	import ru.saveidea.ormtree.manager.ORMTreeManager;
	import ru.saveidea.offline.FileLoader;
	import ru.saveidea.orm.AirDBEntityManager;
	import flash.filesystem.File;

	/**
	 * @author antonsidorenko
	 */
	public class ORMAdminPanelAir extends ORMAdminPanelBase {
		
		private var em : AirDBEntityManager = AirDBEntityManager.instance;
		
		public function ORMAdminPanelAir(settings : ORMAdminPanelAirSettings) {
			super(settings);
		}		
		
		override protected function makeTreeManager() : ORMTreeManager {
			
			var modelForm : ModelForm = new ModelForm((settings as ORMAdminPanelAirSettings).filesFolder);
			
			return new ORMTreeManager(settings.nodeViewClass, modelForm);
		}
		
		override protected function loadDB(db : Object) : void {
			super.loadDB(db);
			
			var dbFile : File = db as File;
			if (!dbFile.exists) {
				trace("File not founded");
				// берём файл по умолчанию и сохраняем его!
				em.dbStorage = dbFile;
				
				findOrMakeRoot();
			} else {
				em.dbStorage = dbFile;
				FileLoader.loadText(dbFile.url, onDBLoadCompleteHandler);
			}
		}
		
		override protected function onDBLoadCompleteHandler(dbContent : String) : void {
			super.onDBLoadCompleteHandler(dbContent);
			
			em.setData(dbContent);

			findOrMakeRoot();
		}

		private function findOrMakeRoot() : void {
			var rootModel : Object;

			if (em.findAll(settings.rootModelClass).length == 0) {
				rootModel = new settings.rootModelClass();
			} else {
				rootModel = em.findAll(settings.rootModelClass)[0];
			}

			data = rootModel;
		}

	}
}