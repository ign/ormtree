package ru.saveidea.ormtree.adminpanel {
	import flash.filesystem.File;

	/**
	 * @author antonsidorenko
	 */
	public class ORMAdminPanelAirSettings extends ORMAdminPanelBaseSettings {
		
		public var filesFolder : File;
		
		public function ORMAdminPanelAirSettings(db : File, rootModelClass : Class, filesFolder : File, nodeViewClass : Class = null, title : String = "Admin Panel") {
			super(db, rootModelClass, nodeViewClass, title);
			
			this.filesFolder = filesFolder;
		}
		
	}
}