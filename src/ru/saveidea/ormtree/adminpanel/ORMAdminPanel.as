package ru.saveidea.ormtree.adminpanel {
	import ru.saveidea.orm.forms.ModelFormBase;
	import ru.saveidea.offline.FileLoader;
	import ru.saveidea.orm.DBEntityManager;
	import ru.saveidea.ormtree.manager.ORMTreeManager;

	/**
	 * @author antonsidorenko
	 */
	public class ORMAdminPanel extends ORMAdminPanelBase {
		
		private var em : DBEntityManager = DBEntityManager.instance;

		public function ORMAdminPanel(settings : ORMAdminPanelSettings) {
			super(settings);
		}

		override protected function makeTreeManager() : ORMTreeManager {			
			var modelForm : ModelFormBase = new ModelFormBase();
			
			return new ORMTreeManager(settings.nodeViewClass, modelForm);
		}

		override protected function loadDB(db : Object) : void {
			super.loadDB(db);

			FileLoader.loadText(db as String, onDBLoadCompleteHandler);
		}

		override protected function onDBLoadCompleteHandler(dbContent : String) : void {
			super.onDBLoadCompleteHandler(dbContent);

			em.setData(dbContent);

			var rootModel : Object;

			if (em.findAll(settings.rootModelClass).length == 0) {
				rootModel = new settings.rootModelClass();
			} else {
				rootModel = em.findAll(settings.rootModelClass)[0];
			}

			data = rootModel;
		}
	}
}