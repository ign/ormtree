package ru.saveidea.utils
{
	import flash.geom.Point;

	public class TreeUtils
	{
		public function TreeUtils()
		{
		}
		
		public static function getDistanceWithoutSqrt(p1:Point,p2:Point):Number
		{
			return Math.pow(p1.x-p2.x,2)+Math.pow(p1.y-p2.y,2);
		}
	}
}