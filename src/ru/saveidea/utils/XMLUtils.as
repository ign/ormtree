package ru.saveidea.utils {

	/**
	 * @author ign
	 */
	public class XMLUtils {

		public static function makeXMLSafety(text : String,error : String = 'XML не валиден') : XML {
			var xml : XML;
			try {
				xml = new XML(text);
			} catch (e : Error) {
				throw new Error(error + '(' + text + ')');
			}
			return xml;
		}
	}
}
