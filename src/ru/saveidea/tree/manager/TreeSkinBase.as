package ru.saveidea.tree.manager
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	import ru.saveidea.ormtree.view.ORMSkinManager;
	
	public class TreeSkinBase extends Sprite
	{
		private var skin:DisplayObject;
		public function TreeSkinBase()
		{
			super();
			skin=new (ORMSkinManager.instance.getSkinById(ORMSkinManager.TREE_BACKGROUND_SKIN) as Class)();
			addChild(skin);
		}
		
		override public function set height(val:Number):void
		{
			skin.height=val;
		}
		
		override public function set width(val:Number):void
		{
			skin.width=val;
		}
	}
}