package ru.saveidea.tree.manager {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.FileReference;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import ru.saveidea.minimalcomps.components.ScrollPane;
	import ru.saveidea.minimalcomps.components.skins.ORMPanelSkin;
	import ru.saveidea.orm.AirDBEntityManager;
	import ru.saveidea.tree.view.TreeNodeViewBaseEvent;
	import ru.saveidea.tree.view.TreeView;
	import ru.saveidea.tree.view.TreeViewEvent;

	/**
	 * @author antonsidorenko
	 */
	public class TreePanel extends ScrollPane {
		
		private var ui : TreePanelUI;
		public var treeView : TreeView;
		private var _data : Object;
		
		private var fileReference : FileReference;
		private var ref_skin:TreeSkinBase;
		private var idTimeout:Number;
		public function TreePanel(treeNodeClass : Class) {
			super();
			autoHideScrollBar = true;

			dragContent = false;
			skinClass=ORMPanelSkin;
			ref_skin=new TreeSkinBase();
			//addChild(ref_skin);
			//color=0xFF;
			ui = new TreePanelUI();
			addRawChild(ui);

			treeView = new TreeView(treeNodeClass);
			// treeView.addEventListener(TreeViewEvent.CHANGE, onTreeViewChangeHandler);
			treeView.addEventListener(TreeViewEvent.NODE_ADD, onNodeAddHandler);
			treeView.addEventListener(TreeViewEvent.NODE_SELECT, onNodeSelectHandler);
			treeView.addEventListener(TreeNodeViewBaseEvent.UPDATE_VIEW, onNeedUpdatePanel);
			addChild(treeView);
			treeView.y = 35;
			// treeView.x = 5;

			// ui.addFolderButton.buttonMode = true;
			// ui.addDocumentButton.buttonMode = true;
			ui.saveButton.buttonMode = true;
			ui.exportButton.buttonMode = true;
			ui.importButton.buttonMode = true;

			// ui.addFolderButton.addEventListener(MouseEvent.CLICK, onAddFolderButtonClickHandler);
			// ui.addDocumentButton.addEventListener(MouseEvent.CLICK, onAddDocumentButtonClickHandler);
			ui.saveButton.addEventListener(MouseEvent.CLICK, onSaveButtonClickHandler);
			ui.exportButton.addEventListener(MouseEvent.CLICK, onExportButtonClickHandler);
			ui.importButton.addEventListener(MouseEvent.CLICK, onImportButtonClickHandler);

			addEventListener(MouseEvent.CLICK, onClickHandler);

			width = 250;
			height = 500;
			
			this.addEventListener(Event.RESIZE,onResizeTree);
			this.addEventListener(Event.ADDED_TO_STAGE,onAddedToStage)
		}
		
		protected function onNeedUpdatePanel(event:Event):void
		{
			clearTimeout(idTimeout);
			idTimeout=setTimeout(onTimeout,500);
			
		}
		
		protected function onAddedToStage(event:Event):void
		{
			stage.addEventListener(TreeNodeViewBaseEvent.EXPANDED,onExpanded);
			stage.addEventListener(TreeNodeViewBaseEvent.COLLAPSED,onCollapsed);
		}
		
		protected function onCollapsed(event:Event):void
		{
			clearTimeout(idTimeout);
			idTimeout=setTimeout(onTimeout,500);
		}
		
		protected function onExpanded(event:Event):void
		{
			clearTimeout(idTimeout);
			idTimeout=setTimeout(onTimeout,500);
		}
		
		private function onTimeout():void
		{
			clearTimeout(idTimeout);
			invalidate();
		}
		
		override protected function addChildren():void
		{
			super.addChildren();
			//content.addChild(_mask);
		}
		
		protected function onResizeTree(event:Event):void
		{
			if (ref_skin)
			{
				ref_skin.width=width;
				ref_skin.height=height;
			}
		}
		
		private function onImportButtonClickHandler(event : MouseEvent) : void {
			// открываем файл менеджер, чтобы загрузить файл базы данных
			fileReference = new FileReference();
			fileReference.addEventListener(Event.SELECT, onFileSelectHandler);
			fileReference.browse();
		}

		private function onFileSelectHandler(event : Event) : void {
			fileReference.addEventListener(Event.COMPLETE, onFileLoadComplete);
			fileReference.load();
		}

		private function onFileLoadComplete(event : Event) : void {
			trace(fileReference.data);
		}

		private function onNodeSelectHandler(event : TreeViewEvent) : void {
			trace("TreePanel.onNodeSelectHandler(event)");
			dispatchEvent(new TreePanelEvent(TreePanelEvent.SELECT_NODE, event.nodeData));
		}

		private function onNodeAddHandler(event : TreeViewEvent) : void {
			dispatchEvent(new TreePanelEvent(TreePanelEvent.MAKE_NODE, event.nodeData));
			
			clearTimeout(idTimeout);
			idTimeout=setTimeout(onTimeout,500);
		}		
		
		private function onTreeViewChangeHandler(event : TreeViewEvent) : void {
			update();
		}

		private function onExportButtonClickHandler(event : MouseEvent) : void {
			save();
			AirDBEntityManager.instance.export();
		}

		private function onClickHandler(event : MouseEvent) : void {
			//treeView.deselect();
		}

		private function onSaveButtonClickHandler(event : MouseEvent) : void {
			save();
		}

		private function save() : void {
			dispatchEvent(new TreePanelEvent(TreePanelEvent.SAVE, null));
			AirDBEntityManager.instance.save(_data);
		}

		public function set data(data : Object) : void {
			_data = data;
			treeView.data = data;
		}

		public function updateView() : void {
			treeView.update();
		}

		override public function draw() : void 
		{
			super.draw();
			_hScrollbar.visible=false;
		}
		/*override public function set width(w : Number) : void {
			super.width = w;
			treeView.width = w;
		}*/
	}
}