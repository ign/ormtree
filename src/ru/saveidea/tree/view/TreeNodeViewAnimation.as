package ru.saveidea.tree.view
{
	import caurina.transitions.Tweener;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import ru.saveidea.tree.manager.TreeManager;
	import ru.saveidea.utils.TreeUtils;

	public class TreeNodeViewAnimation extends TreeNodeViewBase
	{
		
		private var isNeedUpdatePosition:Boolean;
		private var oldY:Number=0;
		private var isIgnoreAnimation:Boolean=false;
		
		//for drag node
		private var dragNode:TreeNodeViewAnimation;
		private var lastOverIndex:int=-1;
		private var overIndex:int=-1;
		private var lastOverNode:TreeNodeViewAnimation;
		private var lastMouseYForDrag:Number=0;
		private var lastMouseDirForDrag:int=0;
		
		private var isDragMouseMoved:Boolean=false;
		private var aux_drag_array:Array;
		private var forDragPoint:Point;
		
		public function TreeNodeViewAnimation(parentNodeView:TreeNodeViewBase)
		{
			super(parentNodeView);
			isNeedUpdatePosition=false;
			aux_drag_array=new Array();
			forDragPoint=new Point();
		}
		
		private function startAnimation():void
		{
			if (!isIgnoreAnimation)
			{
				//if (isNeedUpdatePosition)
				{
					isNeedUpdatePosition=false;
					var toY:Number=this.y;
					this.y=oldY;
					Tweener.addTween(this,{y:toY,time:TreeAnimationNodeSettings.TREE_ANIMATION_TIME}); 
				}
				var child : TreeNodeViewAnimation;
				var childPosition : Number = 0;
				for (var i : int = 0; i < childs.length; i++) {
					child = childs[i];
					if(child==dragNode)
						continue;
					child.startAnimation();
				}
			}
			isIgnoreAnimation=false;
		}
		
		override public function update() : void {
			// пробегаю по списку детей и правильно расставляю их
			var child : TreeNodeViewAnimation;
			var childPosition : Number = 0;
			for (var i : int = 0; i < childs.length; i++) {
				child = childs[i];
				child.update();
				child.x = 0;	
				
				child.oldY=child.y;
				child.y=childPosition;
				
				childPosition += child.height ;
			}
			
			skin.update();
		}
		
		override public function expand(needToUpdateTree : Boolean = true) : void 
		{
			var isOldCollapsed:Boolean=_isCollapsed;
			if (isOldCollapsed)
				isIgnoreAnimation=true;
			
			super.expand(needToUpdateTree);
			
			if (isOldCollapsed)
			{
				var toY:Number=childsContainer.y;
				childsContainer.y-=TreeAnimationNodeSettings.TREE_ANIMATION_DSTY;
				childsContainer.alpha=0;
				Tweener.addTween(childsContainer,{alpha:1,y:toY,time:TreeAnimationNodeSettings.TREE_ANIMATION_TIME});
			}
		}
		
		override public function collapse(needToUpdateTree : Boolean = true) : void 
		{
			var isOldCollapsed:Boolean=_isCollapsed;
			if (!isOldCollapsed)
				isIgnoreAnimation=true;
			
			super.collapse(needToUpdateTree);
			if (!isOldCollapsed)
			{
				addChild(childsContainer);
				Tweener.addTween(childsContainer,{alpha:0,
					y:childsContainer.y-TreeAnimationNodeSettings.TREE_ANIMATION_DSTY,
					time:TreeAnimationNodeSettings.TREE_ANIMATION_TIME,
					onComplete:tweenerCollapseOnComplete});		
			}
		}
		
		override protected function updateTree() : void 
		{
			super.updateTree();
			(getRootView() as TreeNodeViewAnimation).startAnimation();
		}
		
		private function tweenerCollapseOnComplete():void
		{
			removeChild(childsContainer);
		}
		
		
		protected function makeForDragAndDropNodes():void
		{
			this.addEventListener(MouseEvent.MOUSE_DOWN,onMouseForDragNode);
			this.addEventListener(MouseEvent.MOUSE_MOVE,onMouseForDragNode);
			if (stage)
				stage.addEventListener(MouseEvent.MOUSE_UP,onMouseForDragNode);
			else
				this.addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			stage.addEventListener(MouseEvent.MOUSE_UP,onMouseForDragNode);
			this.removeEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
		}
				
		private function chanegPositionForOverNode():void
		{
			var sel_index:int = overIndex;//Math.round(dragNode.y/(dragNode.skin.height));
			var null_index:int=-1;
			var dir:int=0;
			
			if (lastOverIndex==sel_index)
				return;
			lastOverIndex=sel_index;
				
			for (var i:uint=0;i<aux_drag_array.length;++i)
			{
				if (!aux_drag_array[i])
				{
					null_index=i;
					break;
				}
			}
			
			aux_drag_array.splice(null_index,1);
			if (sel_index>=aux_drag_array.length)
			{
				aux_drag_array.push(null);
			}else
				aux_drag_array.splice(sel_index,0,null);
				
			
			var calc_height:Number=0;
			for (i=0;i<aux_drag_array.length;++i)
			{
				if (aux_drag_array[i])
				{
					var toY:Number=calc_height;
					
					//if (Math.abs(toY-aux_drag_array[i].y)>1)
					{
						Tweener.removeTweens(aux_drag_array[i]);
						Tweener.addTween(aux_drag_array[i],{y:toY,
							time:TreeAnimationNodeSettings.TREE_ANIMATION_TIME
						});
					}
					
					calc_height+=aux_drag_array[i].height+1;
				}else
					calc_height+=dragNode.skin.height+1;
			}
		}

		
		protected function onMouseForDragNode(event:MouseEvent):void
		{		
			switch(event.type)
			{
				case MouseEvent.MOUSE_DOWN:
				{
					if (!dragNode)
					{
						for (var i:uint=0;i<childs.length;++i)
						{
							if (childs[i].skin.hitTestPoint(stage.mouseX,stage.mouseY))
							{
								dragNode=childs[i] as TreeNodeViewAnimation;
								if (dragNode.isCollapsed)
								{
									isDragMouseMoved=false;
									forDragPoint.x=stage.mouseX;
									forDragPoint.y=stage.mouseY;
									break;
								}else
									dragNode=null;
							}
						}
					}
				}
					break;
				case MouseEvent.MOUSE_MOVE:
					if (!isDragMouseMoved && dragNode)
					{
						if (TreeUtils.getDistanceWithoutSqrt(forDragPoint,new Point(stage.mouseX,stage.mouseY))>4)
						{
							aux_drag_array.length=0;
							dragNode.startDrag(false,new Rectangle(dragNode.x,0,dragNode.x,childsContainer.height-dragNode.skin.height));
							dragNode.alpha=0.4;
							dragNode.deselect(); 
							isDragMouseMoved=true;
							lastMouseDirForDrag=0;
							lastMouseYForDrag=stage.mouseY;
							
							for (i=0;i<childs.length;++i)
							{
								if (childs[i]===dragNode)
									aux_drag_array[i]=null;
								else
									aux_drag_array[i]=childs[i];
							}
						}
					}
					
					
					if (dragNode)
					{
						var currDir:int = (stage.mouseY - lastMouseYForDrag)>0?1:-1;
						lastMouseYForDrag=stage.mouseY;
						if (currDir!=lastMouseDirForDrag)
						{
							lastOverNode=null;
						}
						lastMouseDirForDrag=currDir;
						
						for (i=0;i<aux_drag_array.length;++i) 
						{
							if (aux_drag_array[i] && aux_drag_array[i]!=dragNode && aux_drag_array[i]!=lastOverNode && aux_drag_array[i].hitTestPoint(stage.mouseX,stage.mouseY))
							{
								overIndex=i;
								lastOverNode=aux_drag_array[i];
								chanegPositionForOverNode();
								break;
							}
						}
					}
					break;
				case MouseEvent.MOUSE_UP:
					if (dragNode && isDragMouseMoved)
					{
						swapNodes();
						dragNode.stopDrag();
						dragNode.alpha=1;
						dragNode.select();
						dragNode=null;
						lastOverIndex=-1;
						overIndex=-1;
						isDragMouseMoved=false;
						updateTree();
					}else
						dragNode=null;
					
					break;
			}	
		}
		
		protected function onSwapNodes(drag_index:uint,sel_index:uint):void
		{
			childs.splice(drag_index,1);
			childs.splice(sel_index,0,dragNode);
		}
		
		private function swapNodes():void
		{			
			var dragIndex:int=getNodeIndex(dragNode);
			var selIndex:int = overIndex;//Math.round(dragNode.y/(dragNode.skin.height));
			if (dragIndex>=0 && selIndex>=0)
			{				
				onSwapNodes(dragIndex,selIndex);
			}
		}
	}
}