package ru.saveidea.tree.view {
	import flash.events.Event;

	/**
	 * @author antonsidorenko
	 */
	public class TreeViewEvent extends Event {
		
		public static const UPDATE : String = "UPDATE";
		
		public static const NODE_SELECT : String = "NODE_SELECT";
		public static const NODE_ADD : String = "NODE_ADD";
		
		public static const NODE_CUSTOM_EVENT : String = "NODE_CUSTOM_EVENT";
		
		public var nodeData : Object;
		
		//Нужно для дополнительных событий от самих TreeNodeView
		public var data : Object;

		public function TreeViewEvent(type : String, _nodeData : Object=null, _data : Object=null, bubbles : Boolean = false, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);

			nodeData = _nodeData;
			data = _data;
		}
	}
}