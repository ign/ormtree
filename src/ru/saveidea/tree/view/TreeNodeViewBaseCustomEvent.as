package ru.saveidea.tree.view {
	import flash.events.Event;

	/**
	 * @author antonsidorenko
	 */
	public class TreeNodeViewBaseCustomEvent extends Event {
		
		public static const CUSTOM_EVENT : String = "CUSTOM_EVENT";
		
		public var nodeData : Object;
		public var data : Object;
		
		public function TreeNodeViewBaseCustomEvent(type : String, _nodeData : Object, _data : Object, bubbles : Boolean = false, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);
			
			nodeData = _nodeData;
			data = _data;
		}
	}
}