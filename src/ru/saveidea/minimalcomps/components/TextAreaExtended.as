package ru.saveidea.minimalcomps.components {
	import caurina.transitions.Tweener;
	
	import com.bit101.components.Panel;
	import com.bit101.components.TextArea;
	import com.mobilewish.as3texteditorlite.AS3TextEditorLite;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	
	import ru.saveidea.minimalcomps.components.skins.ORMHSliderSkin;
	import ru.saveidea.minimalcomps.components.skins.ORMPanelInputTextSKin;
	import ru.saveidea.minimalcomps.components.skins.ORMVSliderSkin;

	/**
	 * @author antonsidorenko
	 */
	public class TextAreaExtended extends TextArea {
		private var for_input_ignore_flag:Boolean=false;
		private var text_editor:AS3TextEditorLite;
		private var text_editor_container:AS3TextEditorSkinContainer;
		public var button:ORMButton;
		private var panel:Panel;
		public function TextAreaExtended(parent : DisplayObjectContainer = null, xpos : Number = 0, ypos : Number = 0, text : String = "") {
			super(parent, xpos, ypos, text);
		}
		override protected function addChildren():void
		{
			super.addChildren();
			button=new ORMButton();
			addChild(button);
			button.label="Show text editor";
			button.addEventListener(MouseEvent.MOUSE_UP,onMouseUp)
		}
		
		public function setButtonVisible(val:Boolean):void
		{
			if (val && !button.parent)
				button.parent.addChild(button)
			else
			if (!val && button.parent)
				button.parent.removeChild(button);
			
		}
		
		override protected function skinPartAdded(part:String, instance:Object):void
		{
			super.skinPartAdded(part,instance);
		
			switch (part)
			{
				case "textField":
					_tf.addEventListener(Event.CHANGE,onChangeForIgnore)
					html=true;
					//editable=true;
				break;
				case "scrollBar":
					_scrollbar.tabChildren = false;
					_scrollbar.tabEnabled = false;
					_scrollbar.skinClass=ORMVSliderSkin;
				break;
				default:
					panel=instance as Panel;
					
					if (panel)
						panel.skinClass=ORMPanelInputTextSKin;
				break;
			}
			
			dispatchEvent(new Event("SKIN_PART_ADDED"))
		}
		
		protected function onChangeForIgnore(event:Event):void
		{
			for_input_ignore_flag=true;
			
		}
		override public function draw():void
		{
			//com.bit101.components.TextArea draw method:
			_tf.width = _width - _scrollbar.width - 4;
			_scrollbar.x = _width - _scrollbar.width;
			_scrollbar.height = _height;
			_scrollbar.draw();
			addEventListener(Event.ENTER_FRAME, onTextScrollDelay);
			
			//com.bit101.components.Text draw method:
			//_panel.setSize(_width, _height);
			//_panel.draw();
			
			_tf.width = _width - 4;
			_tf.height = _height - 4;
			if (for_input_ignore_flag)
			{
				
			}else
			{
				if(_html)
				{
					_tf.htmlText = text;
				}
				else
				{		
					_tf.text = text;
				}
			}
			if(_editable)
			{
				_tf.mouseEnabled = true;
				_tf.selectable = true;
				_tf.type = TextFieldType.INPUT;
			}
			else
			{
				_tf.mouseEnabled = _selectable;
				_tf.selectable = _selectable;
				_tf.type = TextFieldType.DYNAMIC;
			}
			
			if (skin)
			{
				button.width=width;
				button.y=skin.y+panel.height;
				button.x=skin.x
			}
			//_tf.setTextFormat(_format);
		}
				
		protected function onMouseUp(event:MouseEvent):void
		{
			if (!stage)
				return;
			
			if (!text_editor)
				text_editor=new AS3TextEditorLite();
			
			if (!text_editor_container)
				text_editor_container=new AS3TextEditorSkinContainer(text_editor,this);
				
			
			text_editor_container.x=textField.parent.x;
			stage.addChild(text_editor_container)
		}
		
		public function get data() : String {
			if (!text_editor) {
				return null;
			}
			return text_editor.input_txt.htmlText;
		}
		
		override public function set text(t:String):void
		{
			for_input_ignore_flag=false;
			super.text=t;
		}
	}
}
import com.mobilewish.as3texteditorlite.AS3TextEditorLite;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.text.TextField;

import ru.saveidea.minimalcomps.components.ORMButton;
import ru.saveidea.minimalcomps.components.TextAreaExtended;

class AS3TextEditorSkinContainer extends Sprite
{
	private var ref_editor:AS3TextEditorLite;
	private var rect:Rectangle;
	private var textArea:TextAreaExtended;
	private var button:ORMButton;
	private var target:TextAreaExtended;
	public function AS3TextEditorSkinContainer(ref_editor:AS3TextEditorLite,ref_textField:TextAreaExtended)
	{
		this.ref_editor=ref_editor;
		addChild(ref_editor);
		ref_editor.x=ref_editor.y;
		rect=ref_editor.getBounds(null);
		rect.height-=75;
		rect.width-=70;
		textArea=new TextAreaExtended();
		addChild(textArea);
		textArea.setButtonVisible(false);
		
		button=new ORMButton();
		addChild(button);
		button.label="Accept";
		button.addEventListener(MouseEvent.MOUSE_UP,onMouseUp);
		
		this.addEventListener(Event.ADDED_TO_STAGE,onAdded);
		this.addEventListener(Event.REMOVED_FROM_STAGE,onRemoved);
		
		target=ref_textField;		
		textArea.addEventListener("SKIN_PART_ADDED",onPartAdded)
	}
	
	protected function onPartAdded(event:Event):void
	{
		if (textArea.textField)
			ref_editor.input_txt=textArea.textField;
	}
	
	protected function onMouseUp(event:MouseEvent):void
	{
		target.text=textArea.textField.htmlText;
		this.parent.removeChild(this);
	}
	
	private function redrawRect(w:Number,h:Number):void
	{
		this.graphics.clear();
		this.graphics.beginFill(0x000000,0.7);
		this.graphics.lineStyle(1,0xFFFFFF);
		this.graphics.drawRect(0,0,w,h);
		this.graphics.endFill();
		textArea.width=w;
		textArea.height=h-rect.height-40;
		ref_editor.x=(w-rect.width)*.5
			
		textArea.y=rect.height
		button.y=textArea.y+textArea.height;
		button.x=stage.stageWidth-button.width;
	}
	
	private function onAdded(evt:Event):void
	{
		textArea.html=true;
		textArea.text=target.text;
		stage.addEventListener(Event.RESIZE,onResize);
		redrawRect(stage.stageWidth,stage.stageHeight);
	}
	
	private function onRemoved(evt:Event):void
	{
		stage.removeEventListener(Event.RESIZE,onResize);
	}
	
	private function onResize(evt:Event):void
	{
		redrawRect(stage.stageWidth,stage.stageHeight);
	}
}