package ru.saveidea.minimalcomps.components
{
	import com.bit101.components.List;
	import com.bit101.components.Panel;
	import com.bit101.components.VScrollBar;
	import com.dgrigg.minimalcomps.skins.VScrollBarSkin;
	
	import fl.core.UIComponent;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	
	import ru.saveidea.minimalcomps.components.skins.ORMDropDownSkin;
	import ru.saveidea.minimalcomps.components.skins.ORMListItemSkin;
	import ru.saveidea.minimalcomps.components.skins.ORMVSliderSkin;
	
	
	public class ORMComboBoxList extends List
	{
		private var ignore:Boolean=false;
		public function ORMComboBoxList(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, items:Array=null)
		{
			super(parent, xpos, ypos, items);
			_listItemSkinClass=ORMListItemSkin;
			_listItemHeight=35;
		}
		
		protected override function onScroll(event:Event):void
		{
			if (_scrollbar)
			{
				var visible_count:uint=height/_listItemHeight;
				var contentHeight:Number = _items.length * _listItemHeight - visible_count* _listItemHeight;
				_itemHolder.y =- Math.round(_scrollbar.getScrollPercent()*contentHeight);
			}
		}
		
		override protected function skinPartAdded(part:String, instance:Object):void 
		{
			super.skinPartAdded(part, instance);
			switch (part)
			{
				case "scrollBar":
					_scrollbar.skinClass=ORMVSliderSkin;
					_scrollbar.y=13;
				break;
				case "panel":
					_panel.skinClass=ORMDropDownSkin;
				break;
			}
		}
		
		override protected function addChildren():void
		{
			super.addChildren();
		}
		
		public override function set height(h:Number):void
		{
			super.height=h;
		}
		public override function draw() : void
		{
			
			if (_panel && _panel._mask)
				_panel._mask.y=6;
			
			// scrollbar settings
			_scrollbar.height=_height-10;
			_scrollbar.x=_width-8;
			var contentHeight:Number = _items.length * _listItemHeight;
			_scrollbar.setThumbPercent(_height / contentHeight); 
			var pageSize:Number = _height / _listItemHeight;
			_scrollbar.setSliderParams(0, Math.max(0, _items.length - pageSize), _itemHolder.y / _listItemHeight);
			_scrollbar.pageSize = pageSize;
			
			super.draw();
			
			_selectedIndex = Math.min(_selectedIndex, _items.length - 1);
			
			_panel.height=height+5
			
			_panel._mask.height=_panel.height-12
			_panel.draw();
			if (_panel.skin)
				_panel.skin.drawForce();
			// list items
			makeListItems();
			scrollToSelection();
			
			_panel.y=3;
		}
	}
}