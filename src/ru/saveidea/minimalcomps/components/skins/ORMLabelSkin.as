package ru.saveidea.minimalcomps.components.skins
{
	import com.bit101.components.Label;
	import com.bit101.components.Style;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class ORMLabelSkin extends Skin
	{
		public var label:TextField;
		public function ORMLabelSkin()
		{
			super();
		}
		override protected function createChildren():void
		{
			super.createChildren();
			
			label = new TextField();
			label.name = "label";
			label.height = _height;
			label.embedFonts=true;
			label.selectable = false;
			label.mouseEnabled = false;
			var tf:TextFormat =  new TextFormat(Style.fontName, Style.fontSize, Style.LABEL_TEXT);
			label.defaultTextFormat = tf;
			
			addChild(label);
			
		}
		
		
		override protected function draw():void 
		{
			super.draw();
			
			var host:Label = hostComponent as Label;
			label.text = host.text;
		}
	}
}