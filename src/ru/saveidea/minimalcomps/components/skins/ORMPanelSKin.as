package ru.saveidea.minimalcomps.components.skins
{
	import com.bit101.components.Panel;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	import flash.geom.Rectangle;
	
	public class ORMPanelSkin extends Skin
	{
		private var grid:ORMSkinGrid
		public function ORMPanelSkin()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			grid=new ORMSkinGrid(new Rectangle(5,5,194,74),new SkinForm())
			
			var pnl:Panel =  hostComponent as Panel;
			if (pnl)
			{
				pnl.removeChildAt(0);
				pnl.addChildAt(grid,0);
				pnl.filters=[];
			}else
				hostComponent.removeChildAt(0);
			
			hostComponent.addChildAt(grid,0);
			validate()
		}
		
		override protected function draw():void
		{
			grid.width=hostComponent.width;
			grid.height=hostComponent.height;
		}
	}
}