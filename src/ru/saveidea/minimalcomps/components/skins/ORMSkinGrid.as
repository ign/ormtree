package ru.saveidea.minimalcomps.components.skins
{
	import com.dgrigg.minimalcomps.graphics.Rect;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class ORMSkinGrid extends Sprite
	{
		private var params:Array;
		private var elements:Array;
		private var bitmapData:BitmapData;
		private var rect_grid:Rectangle
		public function ORMSkinGrid(_rect_grid:Rectangle,bData:BitmapData)
		{
			super();
			rect_grid=_rect_grid;
			if (rect_grid.x<1)
				rect_grid.x=1;
			if (rect_grid.y<1)
				rect_grid.y=1;
			if (rect_grid.width>bData.width-2)
				rect_grid.width=bData.width-2;
			if (rect_grid.height>bData.height-2)
				rect_grid.height=bData.height-2;
				
			bitmapData=bData.clone();
			buildingGrid();
		}
		
		override public function set width(val:Number):void
		{
			val=int(val); 
			if (elements.length==9)
			{
			
				elements[1].width=val-elements[0].width-elements[2].width;
				elements[2].x=elements[1].x+elements[1].width;
				
				
				elements[4].width=val-elements[3].width-elements[5].width;
				elements[5].x=elements[4].x+elements[4].width;
				
				
				elements[7].width=val-elements[6].width-elements[8].width;
				elements[8].x=elements[7].x+elements[7].width;
			}
		}
		
				
		override public function set height(val:Number):void
		{
			val=int(val);
			if (elements.length==9)
			{
				elements[3].height=val-elements[0].height-elements[7].height;
				elements[4].height=val-elements[0].height-elements[7].height;
				elements[5].height=val-elements[0].height-elements[7].height;
				elements[6].y=elements[3].y+elements[3].height;
				elements[7].y=elements[3].y+elements[3].height;
				elements[8].y=elements[3].y+elements[3].height;
			}
		}
		
		private function buildingGrid():void
		{
			params=[];
			params.length=0;
			params.push(new Rectangle(0,
										0,
										rect_grid.x,
										rect_grid.y));
			params.push(new Rectangle(rect_grid.x,
										0,
										rect_grid.width,
										rect_grid.y));
			params.push(new Rectangle(rect_grid.x+rect_grid.width,
										0,
										bitmapData.width - rect_grid.width-rect_grid.x,
										rect_grid.y));	
					
			
			
			
				
			params.push(new Rectangle(0,
										rect_grid.y,
										rect_grid.x,
										rect_grid.height));
			params.push(new Rectangle(rect_grid.x,
										rect_grid.y,
										rect_grid.width,
										rect_grid.height));
			params.push(new Rectangle(rect_grid.x+rect_grid.width,
										rect_grid.y,
										bitmapData.width - rect_grid.width-rect_grid.x,
										rect_grid.height));	
			
			
			
			
				
			params.push(new Rectangle(0,
									rect_grid.y+rect_grid.height,
									rect_grid.x,
									bitmapData.height - rect_grid.y-rect_grid.height));
				
			params.push(new Rectangle(rect_grid.x,
										rect_grid.y+rect_grid.height,
										rect_grid.width,
										bitmapData.height - rect_grid.y-rect_grid.height));
				
			params.push(new Rectangle(rect_grid.x+rect_grid.width,
										rect_grid.y+rect_grid.height,
										bitmapData.width - rect_grid.width-rect_grid.x,
										bitmapData.height - rect_grid.y-rect_grid.height));
				
			if (!params.length)
				return;
			if (!bitmapData)
				return;
			elements=new Array();
			
			
			if (params.length ==3)
			{
				for (var i:uint=0;i<params.length;++i)
				{
					var rect:Rectangle=params[i];
					if (rect.width && rect.height)
					{
						var b:Bitmap=new Bitmap();
						var bData:BitmapData=new BitmapData(rect.width,rect.height,true,0);
						bData.copyPixels(bitmapData,new Rectangle(rect.x,rect.y,rect.width,rect.height),new Point(0,0));
						b.bitmapData=bData;
						b.x=rect.x;
						b.y=rect.y;
						elements.push(b);
						addChild(b);
					}
				}
			}else
			if (params.length ==9)
			{
				for (i=0;i<params.length;++i)
				{
					rect=params[i];
					if (rect.width && rect.height)
					{
						b=new Bitmap();
						bData=new BitmapData(rect.width,rect.height);
						bData.copyPixels(bitmapData,new Rectangle(rect.x,rect.y,rect.width,rect.height),new Point(0,0));
						b.bitmapData=bData;
						b.x=rect.x;
						b.y=rect.y;
						elements.push(b);
				
						addChild(b);
					}
				}
			}	
			
		}
	}
}