package ru.saveidea.minimalcomps.components.skins
{
	import com.bit101.components.VScrollSlider;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	
	public class ORMVScrollSliderSkin extends Skin
	{
		private var handle:ORMSkinGrid;
		private var _hitArea:Sprite;
		public function ORMVScrollSliderSkin()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			hostComponent.width=4;
			
			handle=new ORMSkinGrid(new Rectangle(1,10,2,113),new VSliderSkin());
			
			handle.name = "handle";
			handle.buttonMode = true;
			handle.useHandCursor = true;
			
			handle.height=70;
			
			_hitArea=new Sprite()
			handle.addChild(_hitArea);
			_hitArea.graphics.beginFill(0);
			_hitArea.graphics.drawRect(0,0,1,1);
			_hitArea.graphics.endFill();
			_hitArea.alpha=0;
			
			addChild(handle);
			invalidate();
			
		}
		
		override protected function draw():void 
		{
			super.draw();
			if (handle)
			{
				var host:VScrollSlider = hostComponent as VScrollSlider;
				if (host)
				{
					handle.height = (host.getThumbPercent() * height)-5;
				
					_hitArea.x=-4
					_hitArea.width=12;
					_hitArea.height=(host.getThumbPercent() * height)-5;
				}
			}
			
		}
	}
}