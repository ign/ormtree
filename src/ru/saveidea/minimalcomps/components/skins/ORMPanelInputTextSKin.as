package ru.saveidea.minimalcomps.components.skins
{
	import com.bit101.components.InputText;
	import com.bit101.components.Panel;
	import com.bit101.components.Text;
	import com.dgrigg.minimalcomps.events.SkinPartEvent;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	public class ORMPanelInputTextSKin extends Skin
	{
		private var grid:ORMSkinGrid;
		private var grid_active:ORMSkinGrid;
		private var target_for_focus_events:DisplayObject;
		public function ORMPanelInputTextSKin()
		{
			super();
				//addChild(grid);
			this.addEventListener(Event.REMOVED_FROM_STAGE,onRemovedFromStage);
		}
		
		protected function onRemovedFromStage(event:Event):void
		{
			if (grid.parent)
				grid.parent.removeChild(grid);
			if (grid_active.parent)
				grid_active.parent.removeChild(grid_active);
			this.removeEventListener(Event.REMOVED_FROM_STAGE,onRemovedFromStage);
			
			if (target_for_focus_events)
			{
				target_for_focus_events.removeEventListener(FocusEvent.FOCUS_IN,onFocusIn);
				target_for_focus_events.removeEventListener(FocusEvent.FOCUS_OUT,onFocusOut);
			}
		}
		
		override protected function createChildren():void
		{
			grid=new ORMSkinGrid(new Rectangle(5,5,556,102),new SkinTextInput())
			grid_active=new ORMSkinGrid(new Rectangle(5,5,194,22),new SkinTextInputActive())
				
			var pnl:Panel =  hostComponent as Panel;
			if (pnl)
			{
				pnl.removeChildAt(0);
				pnl.addChildAt(grid,0);
				pnl.filters=[];
			}else
				hostComponent.removeChildAt(0);
			
			hostComponent.addChildAt(grid,0);
			hostComponent.addChildAt(grid_active,0);
			grid_active.visible=false;
			
			var text:InputText =hostComponent as InputText;
			if (text)
			{
				target_for_focus_events=text.textField;
				text.textField.addEventListener(FocusEvent.FOCUS_IN,onFocusIn);
				text.textField.addEventListener(FocusEvent.FOCUS_OUT,onFocusOut);
			}
			if (!text)
			{
				var text_area:Text = hostComponent.parent.parent as Text;
				if (text_area)
				{
					if (text_area.textField)
					{
						target_for_focus_events=text_area.textField;
						text_area.textField.addEventListener(FocusEvent.FOCUS_IN,onFocusIn);
						text_area.textField.addEventListener(FocusEvent.FOCUS_OUT,onFocusOut);
					}else
					{
						text_area.skin.addEventListener(SkinPartEvent.PART_ADDED,onPartAdded)
					}
				}
			}
			
			
			validate()
		}
		
		protected function onPartAdded(event:SkinPartEvent):void
		{
			if (event.instance as TextField)
			{
				target_for_focus_events=event.instance as DisplayObject;
				event.instance.addEventListener(FocusEvent.FOCUS_IN,onFocusIn);
				event.instance.addEventListener(FocusEvent.FOCUS_OUT,onFocusOut);
			}
			
		}
		
		protected function onFocusOut(event:FocusEvent):void
		{
			grid_active.visible=false;
			grid.visible=true;
		}
		
		protected function onFocusIn(event:FocusEvent):void
		{
			grid_active.visible=true;
			grid.visible=false;
		}
		
		override protected function draw():void
		{
			grid.width=hostComponent.width;
			grid.height=hostComponent.height;
			
			grid_active.width=hostComponent.width;
			grid_active.height=hostComponent.height;
		}
	}
}