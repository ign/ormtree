package ru.saveidea.minimalcomps.components.skins 
{
	import com.bit101.components.Style;
	import com.dgrigg.minimalcomps.graphics.Rect;
	import com.dgrigg.minimalcomps.graphics.SolidFill;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	public class ORMCheckBoxSkin extends Skin
	{
		
		public var back:Rect;
		public var button:Rect;
		private var skin_check_box:SkinCheckBox;
		public function ORMCheckBoxSkin()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			/*back = new Rect();
			
			var fill:SolidFill = new SolidFill();
			fill.color = Style.BACKGROUND;
			back.fill = fill;
			back.width = 10;
			back.height = 10;
			back.filters = [getShadow(2, true)];
			addChild(back);
			
			button = new Rect();
			button.x = 2;
			button.y = 2;
			button.height = 6;
			button.width = 6;
			fill = new SolidFill();
			fill.color = Style.BUTTON_FACE;
			button.fill = fill;
			button.filters = [getShadow(1)];
			button.visible = false;
			addChild(button);*/
			skin_check_box=new SkinCheckBox();
			addChild(skin_check_box);
			skin_check_box.gotoAndStop(1);
			hostComponent.width=skin_check_box.width;
			hostComponent.height=skin_check_box.height
		}
		
		
		override protected function draw():void 
		{
			super.draw();
			
			skin_check_box.gotoAndStop(currentState == "selected"?2:1);
			//button.visible = currentState == "selected";
		}
	}
}