package ru.saveidea.minimalcomps.components.skins
{
	import com.bit101.components.Component;
	import com.bit101.components.HScrollSlider;
	import com.bit101.components.Style;
	import com.dgrigg.minimalcomps.graphics.Rect;
	import com.dgrigg.minimalcomps.graphics.SolidFill;
	import com.dgrigg.minimalcomps.skins.HScrollSliderSkin;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.geom.Rectangle;
	
	public class ORMHSliderSkin extends Skin
	{
	
		private var scrollSlider:HScrollSlider;
		public function ORMHSliderSkin()
		{
			super();
		}
				
		override protected function createChildren():void
		{
			super.createChildren(); 
	
			scrollSlider = new HScrollSlider();
			scrollSlider.name = "scrollSlider";
			scrollSlider.skinClass= ORMHScrollSliderSkin;
			addChild(scrollSlider);
			scrollSlider.visible=false;
			
			
			
			invalidate();	
		}
		
		override protected function draw():void 
		{
			super.draw();
			
			if (scrollSlider)
			{
				
				scrollSlider.visible = (scrollSlider.getThumbPercent()>=0.999)?false:true;
				scrollSlider.width=width;
				scrollSlider.validate();
			}
			
		}
	}
}