package ru.saveidea.minimalcomps.components.skins
{
	import com.bit101.components.Panel;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	import flash.geom.Rectangle;
	
	public class ORMDropDownSkin extends Skin
	{
		private var grid:ORMSkinGrid;
		public function ORMDropDownSkin()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			grid=new ORMSkinGrid(new Rectangle(30,10,83,79),new SkinDropDown())
			
			var pnl:Panel =  hostComponent as Panel;
			if (pnl)
			{
				pnl.removeChildAt(0);
				pnl.addChildAt(grid,0);
				pnl.filters=[];
				pnl.visible=false;
			}
			this.visible=false;
		} 
		 
		override protected function draw():void
		{
			var pnl:Panel =  hostComponent as Panel;
			grid.width=pnl.width;
			grid.height=pnl.height;
			pnl.visible=true;
			this.visible=true;
		}
	}
}