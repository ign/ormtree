package ru.saveidea.minimalcomps.components.skins
{
	import com.bit101.components.RadioButton;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	public class ORMRadioButtonSKin extends Skin
	{
		private var skin_radio_button:SkinRadioButton;
		public function ORMRadioButtonSKin()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();	
			skin_radio_button=new SkinRadioButton();
			addChild(skin_radio_button);
			skin_radio_button.gotoAndStop(1);
		}
		
		protected override function draw():void
		{
			super.draw();
			var rb:RadioButton =  hostComponent as RadioButton
			skin_radio_button.gotoAndStop(rb.selected?2:1);
		}
	}
}