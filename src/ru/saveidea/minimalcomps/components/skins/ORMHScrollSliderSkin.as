package ru.saveidea.minimalcomps.components.skins
{
	import com.bit101.components.HScrollSlider;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public class ORMHScrollSliderSkin extends Skin
	{
		private var handle:ORMSkinGrid;
		private var _hitArea:Sprite;
		public function ORMHScrollSliderSkin()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			handle=new ORMSkinGrid(new Rectangle(10,1,123,2),new HSliderSkin());
			
			handle.name = "handle";
			handle.buttonMode = true;
			handle.useHandCursor = true;
			addChild(handle); 
			
			_hitArea=new Sprite()
			handle.addChild(_hitArea);
			_hitArea.graphics.beginFill(0);
			_hitArea.graphics.drawRect(0,0,1,1);
			_hitArea.graphics.endFill();
			_hitArea.alpha=0;
			
			handle.width=70;
			 
			hostComponent.height=4;
			invalidate();
						
		}
		
		override protected function draw():void 
		{
			super.draw();
			
			if (handle)
			{ 
				var host:HScrollSlider = hostComponent as HScrollSlider;
				//handle.width = width-2;
				handle.width = (host.getThumbPercent() * width)-5;
				_hitArea.y=-4
				_hitArea.height=12;
				_hitArea.width=(host.getThumbPercent() * width)-5;
			}
			
		}
	}
}