package ru.saveidea.minimalcomps.components.skins
{
	import com.bit101.components.VScrollSlider;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	import mx.managers.HistoryManager;
	
	public class ORMVSliderSkin extends Skin
	{
		private var scrollSlider:VScrollSlider;
		
		public function ORMVSliderSkin()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			super.createChildren(); 
			
			scrollSlider = new VScrollSlider();
			scrollSlider.name = "scrollSlider";
			scrollSlider.skinClass= ORMVScrollSliderSkin;
			scrollSlider.width=4;
			addChild(scrollSlider);
			scrollSlider.visible=false;
			
			
			invalidate();	
		}
		
		override protected function draw():void 
		{
			super.draw();
			
			if (scrollSlider)
			{
				scrollSlider.visible = (scrollSlider.getThumbPercent()>=0.999)?false:true;
				scrollSlider.height=height;				
				//trace(this.parent,this.parent.parent,this.parent.parent.parent,this.parent.parent.parent.parent,this.parent.parent.parent.parent.parent,this.parent.parent.parent.parent.parent.parent,this.parent.parent.parent.parent.parent.parent.parent)
			}
		}
	}
}