package ru.saveidea.minimalcomps.components.skins 
{
	import com.bit101.components.Component;
	import com.bit101.components.Label;
	import com.bit101.components.ListItem;
	import com.bit101.components.Style;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class ORMListItemSkin extends Skin
	{
		private var label:Label;
		private var back:MovieClip;
		private var selected:Boolean=false;
		private var over:TextFormat;
		private var normal:TextFormat;
		
		private var over_filter:DropShadowFilter;
		private var normal_filter:DropShadowFilter;
		
		public function ORMListItemSkin()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			back=new SkinDropDownItem();
			addChild(back);
			back.ico.visible=false;
			back.ico.gotoAndStop(1)
			back.ico.y+=3;  
			back.ico.y=int(back.ico.y);
			back.ico.x=int(back.ico.x)+0.5;
			
			back.addEventListener(MouseEvent.ROLL_OVER,onRollOver);
			back.addEventListener(MouseEvent.ROLL_OUT,onRollOut);
			
			label = new Label();
			label.name = "label";
			label.skinClass=ORMLabelSkin;
			label.mouseChildren=label.mouseEnabled=false;
			addChild(label);
			
			normal  =  new TextFormat("Helvetica Neue", 14,0x9a9a9a);
			over  =  new TextFormat("Helvetica Neue", 14,0xf56a59);
			
			//over_filter =new DropShadowFilter(1,90,0xf56a59,1,1,1,0.45)
			//normal_filter =new DropShadowFilter(1,90,0x9a9a9a,1,1,1,0.45)
				
			this.visible=false;
			this.buttonMode=true;
		}
		
		protected function onRollOut(event:MouseEvent):void
		{
			selected=false;
			draw()
		}
		
		protected function onRollOver(event:MouseEvent):void
		{
			selected=true;
			draw()
		}
		
		override protected function draw():void 
		{
			super.draw();
			var host:ListItem = hostComponent as ListItem;
			this.visible=true;
			if (label)
			{
				label.textField.embedFonts=true;
				if (selected)
				{
					label.x = 21;
					back.ico.visible=true;
					label.textField.defaultTextFormat=over;
					//label.filters=[over_filter];
				}
				else
				{
					label.x = 16;
					label.textField.defaultTextFormat=normal;
					back.ico.visible=false;
					//label.filters=[normal_filter];
				}
				
				back.y=4; label.y=  12;
				back.line.width=host.width;
				
				if(host.data is String)
				{
					label.text = host.data as String;
				}
				else 
				if(host.data.label is String)
				{
					label.text = host.data.label;
				}
				else
				{
					label.text = String(host.data)
				}
				label.textField.embedFonts=true;
			}
		}
	}
}