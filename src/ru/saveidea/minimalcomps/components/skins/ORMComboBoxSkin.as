package ru.saveidea.minimalcomps.components.skins
{
	import com.bit101.components.ComboBox;
	import com.bit101.components.Label;
	import com.bit101.components.List;
	import com.dgrigg.minimalcomps.skins.Skin;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	import ru.saveidea.minimalcomps.components.ORMComboBoxList;
	
	public class ORMComboBoxSkin extends Skin
	{
		private var label:TextField;
		private var list:ORMComboBoxList;
		private var button:ORMSkinGrid;
		private var button_active:ORMSkinGrid;
		
		private var format:TextFormat;
		private var ico:TreeNodeIcoSkin;
		public function ORMComboBoxSkin()
		{
			super();
			
		}
		
		override protected function onRemoved(event:Event):void 
		{
			if (list.parent)
				list.parent.removeChild(list);
		}
		
		override protected function createChildren():void
		{
			list = new ORMComboBoxList();
			list.name = "list";
			addChild(list);
			
			button=new ORMSkinGrid(new Rectangle(5,5,131,21),new SkinComboboxNormal()); 
			
			button_active=new ORMSkinGrid(new Rectangle(5,5,133,23),new SkinComboboxActive()); 
			
			button.name="dropDownButton";
			addChild(button);
			button.buttonMode=true;
			
			addChild(button_active);
			button_active.visible=false;
			button_active.buttonMode=true;
			
			ico=new TreeNodeIcoSkin();
			addChild(ico);
			ico.y=16;
			ico.gotoAndStop(1);
			ico.rotation=90;
			ico.mouseEnabled=false
			
			
			
			label=new TextField();
			label.autoSize=TextFieldAutoSize.LEFT;
			label.defaultTextFormat=  new TextFormat("Helvetica Neue", 14,0x9a9a9a);
			addChild(label);
			label.text="Select"
			label.mouseEnabled=false;
			label.filters=[new DropShadowFilter(1,90,0xFFFFFF,1,0,0,1)];
			label.embedFonts=true;
			/*labelButton = new PushButton();
			labelButton.name = "labelButton";
			addChild(labelButton);
			
			dropDownButton = new PushButton();
			dropDownButton.name = "dropDownButton";
			addChild(dropDownButton);*/
			this.visible=false;
		
			var host:ComboBox = hostComponent as ComboBox;
			if (host.width< button.width)
				host.width = button.width;
			
			if (host.height< button.height)
				host.height = button.height;
			validate()
		}
		
		/*override protected function invalidate():void 
		{
			draw()
		}*/
		
		override protected function draw():void 
		{
			super.draw();
			if (!label)
				return;
			
			button.width=width;
			if (button.width<20)
				button.width=20;
			button_active.width=width+2;
			button_active.x=-1;
			ico.x=width-20;
			
			if (currentState == "open")
			{
				button.alpha=0;
				button_active.visible=true;
			}
			else
			{
				button.alpha=1;
				button_active.visible=false;
			}
			
			/*labelButton.setSize(width - height + 1, height);
			labelButton.draw();
			
			if (currentState == "open")
			{
				dropDownButton.label = "-";
			}
			else
			{
				dropDownButton.label = "+";
			}
			
			dropDownButton.setSize(height, height);
			dropDownButton.draw();
			dropDownButton.x = width - height;*/
			
			var host:ComboBox = hostComponent as ComboBox;
			list.setSize(width, host.getVisibleNumItems() * host.listItemHeight);
			list.draw();

			if (host && host.selectedItem)
			{
				if(host.selectedItem is String)
				{
					label.text = host.selectedItem as String;
				}else {
					label.text = host.selectedItem.label as String;
				}				
			}
			
			label.x=((width-20) - label.width)*.5;
			label.y=(height - label.height)*.5;
			this.visible=true;
			
		}
	}
}