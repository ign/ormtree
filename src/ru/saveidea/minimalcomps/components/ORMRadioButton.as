package ru.saveidea.minimalcomps.components
{
	import com.bit101.components.Component;
	import com.bit101.components.RadioButton;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	
	public class ORMRadioButton extends RadioButton
	{
		public function ORMRadioButton(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, label:String="", checked:Boolean=false, defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, label, checked, defaultHandler);
		}
		
		public override function draw():void
		{
			dispatchEvent(new Event(Component.DRAW));
			
			if (_skin)
			{
				_skin.currentState = getCurrentSkinState();
				//_skin.invalidate();
			}
		}
	}
}