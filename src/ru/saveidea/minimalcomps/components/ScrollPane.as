package ru.saveidea.minimalcomps.components {
	import com.bit101.components.HScrollBar;
	import com.bit101.components.Panel;
	import com.bit101.components.Style;
	import com.bit101.components.VScrollBar;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	import ru.saveidea.minimalcomps.components.skins.ORMHSliderSkin;
	import ru.saveidea.minimalcomps.components.skins.ORMVSliderSkin;

	/**
	 * @author antonsidorenko
	 */
	public class ScrollPane extends Panel {
		protected var _vScrollbar : VScrollBar;
		protected var _hScrollbar : HScrollBar;
		protected var _corner : Shape;
		protected var _dragContent : Boolean = true;
		protected var _scrollsStride:uint=4;
		/**
		 * Constructor
		 * @param parent The parent DisplayObjectContainer on which to add this ScrollPane.
		 * @param xpos The x position to place this component.
		 * @param ypos The y position to place this component.
		 */
		public function ScrollPane(parent : DisplayObjectContainer = null, xpos : Number = 0, ypos : Number = 0) {
			super(parent, xpos, ypos);
		}

		/**
		 * Initializes this component.
		 */
		override protected function init() : void {
			super.init();
			addEventListener(Event.RESIZE, onResize);
			_background.addEventListener(MouseEvent.MOUSE_DOWN, onMouseGoDown);
			_background.useHandCursor = true;
			_background.buttonMode = true;
			setSize(100, 100);
		}

		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren() : void {
			super.addChildren();
			_vScrollbar = new VScrollBar(null, width - _scrollsStride, 0, onScroll);
			_vScrollbar.width=4;
			_hScrollbar = new HScrollBar(null, 0, height - _scrollsStride, onScroll);
			_hScrollbar.height=4;
			_vScrollbar.skinClass=ORMVSliderSkin;
			_hScrollbar.skinClass=ORMHSliderSkin;
			addRawChild(_vScrollbar); 
			addRawChild(_hScrollbar);
			_vScrollbar.y=_scrollsStride;
			_hScrollbar.x=_scrollsStride; 
			_corner = new Shape();
			_corner.graphics.beginFill(0,0.5);
			_corner.graphics.drawCircle(0, 0, 4);
			_corner.graphics.endFill();
			//addRawChild(_corner);
			
			
		}

		// /////////////////////////////////
		// public methods
		// /////////////////////////////////
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw() : void {
			super.draw();

			
			// TODO:Разобраться, почему вычитается 8 пикселей
			var vPercent : Number = (_height - _scrollsStride) / content.height;
			var hPercent : Number = (_width - _scrollsStride) / content.width;
			
			//MODIFIED
			hPercent = _width / content.width;
			vPercent = _height / content.height;

			_vScrollbar.x = width - (_scrollsStride+_vScrollbar.width);
			_hScrollbar.y = height -(_scrollsStride+_hScrollbar.height);

			if (hPercent >= 1) {
				_vScrollbar.height = height-_scrollsStride*2;
				_mask.height = height;
			} else {
				_vScrollbar.height = height -_scrollsStride*2;
				_mask.height = height;//-_scrollsStride*2;
			}
			if (vPercent >= 1) {
				_hScrollbar.width = width-_scrollsStride*2;
				_mask.width = width;
			} else {
				_hScrollbar.width = width -_scrollsStride*2;
				_mask.width = width;//-_scrollsStride;
			}
			_vScrollbar.setThumbPercent(vPercent);
			_vScrollbar.maximum = Math.max(0, content.height - _height+_scrollsStride*2);
			//MODIFIED
			_vScrollbar.maximum = Math.max(0, content.height - _height+_scrollsStride*2);
			_vScrollbar.pageSize = content.height - _height;

			_hScrollbar.setThumbPercent(hPercent);
			_hScrollbar.maximum = Math.max(0, content.width - _width+_scrollsStride*2);
			//MODIFIED
			_hScrollbar.maximum = Math.max(0, content.width - _width+_scrollsStride*2);
			_hScrollbar.pageSize = content.width - _width;

			_corner.x = width -4;
			_corner.y = height -4;
			_corner.visible = (hPercent < 1) && (vPercent < 1);
			content.x = -_hScrollbar.value;
			content.y = -_vScrollbar.value;
		}

		/**
		 * Updates the scrollbars when content is changed. Needs to be done manually.
		 */
		public function update() : void {
			invalidate();
		}

		// /////////////////////////////////
		// event handlers
		// /////////////////////////////////
		/**
		 * Called when either scroll bar is scrolled.
		 */
		protected function onScroll(event : Event) : void {
			content.x = -_hScrollbar.value;
			content.y = -_vScrollbar.value;
		}

		protected function onResize(event : Event) : void {
			invalidate();
		}

		protected function onMouseGoDown(event : MouseEvent) : void {
			content.startDrag(false, new Rectangle(0, 0, Math.min(0, _width - content.width - 10), Math.min(0, _height - content.height - 10)));
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseGoUp);
		}

		protected function onMouseMove(event : MouseEvent) : void {
			_hScrollbar.value = -content.x;
			_vScrollbar.value = -content.y;
		}

		protected function onMouseGoUp(event : MouseEvent) : void {
			content.stopDrag();
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseGoUp);
		}

		public function set dragContent(value : Boolean) : void {
			_dragContent = value;
			if (_dragContent) {
				_background.addEventListener(MouseEvent.MOUSE_DOWN, onMouseGoDown);
				_background.useHandCursor = true;
				_background.buttonMode = true;
			} else {
				_background.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseGoDown);
				_background.useHandCursor = false;
				_background.buttonMode = false;
			}
		}

		public function get dragContent() : Boolean {
			return _dragContent;
		}

		/**
		 * Sets / gets whether the scrollbar will auto hide when there is nothing to scroll.
		 */
		public function set autoHideScrollBar(value : Boolean) : void {
			_vScrollbar.autoHide = value;
			_hScrollbar.autoHide = value;
		}

		public function get autoHideScrollBar() : Boolean {
			return _vScrollbar.autoHide;
		}
	}
}
