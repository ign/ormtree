package ru.saveidea.minimalcomps.components
{
	import com.bit101.components.PushButton;
	import com.bit101.components.Style;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.SimpleButton;
	import flash.filters.DropShadowFilter;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	import ru.saveidea.minimalcomps.components.skins.ORMSkinGrid;

	public class ORMButton extends PushButton
	{
		private var button_skin:SimpleButton;
		private var shadow:DropShadowFilter;
		private var format:TextFormat;
		private var button_label:TextField;
		
		public function ORMButton(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, label:String="", defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, label, defaultHandler);
			
			skinClass =null;
			skin=null;
			
			if (skin && skin.parent)
				skin.parent.removeChild(skin);
			
			button_skin=new SimpleButton(new ORMSkinGrid(new Rectangle(5,5,72,22),new SkinButtonNormal()),
										new ORMSkinGrid(new Rectangle(5,5,72,20),new SkinButtonOver()),
										new ORMSkinGrid(new Rectangle(5,5,72,20),new SkinButtonClick()),
										new ORMSkinGrid(new Rectangle(5,5,72,20),new SkinButtonClick())
									);
		
			Style		
			addChild(button_skin);  
			
			button_label=new TextField();
			button_label.autoSize=TextFieldAutoSize.LEFT;
			addChild(button_label);
	
			shadow=new DropShadowFilter(1,270,0,0.56,1,1,0.45);
			button_label.filters=[shadow];
			button_label.selectable=false;
			button_label.mouseEnabled=false;
			
				
			format=new TextFormat(Style.fontName,14,0xFFFFFF);
			button_label.defaultTextFormat=format;
			button_label.embedFonts=false;
			button_label.width=300;
			button_label.embedFonts=true;
			button_skin.upState.height=32;
			
			height=30;
			this.visible=false;
		}
		
		public override function set width(val:Number):void
		{
			_width=val;
			var ref_grid:ORMSkinGrid;
			ref_grid = button_skin.overState as ORMSkinGrid;
			ref_grid.width=val;
			
			ref_grid = button_skin.downState as ORMSkinGrid;
			ref_grid.width=val;
			
			ref_grid = button_skin.upState as ORMSkinGrid;
			ref_grid.width=val;
			
			ref_grid = button_skin.hitTestState as ORMSkinGrid;
			ref_grid.width=val;
			
			label=button_label.text;
		}
		
		public override function set height(val:Number):void
		{
			_height=val;
			var ref_grid:ORMSkinGrid;
			ref_grid = button_skin.overState as ORMSkinGrid;
			ref_grid.height=val;
			
			ref_grid = button_skin.downState as ORMSkinGrid;
			ref_grid.height=val;
			
			ref_grid = button_skin.upState as ORMSkinGrid;
			ref_grid.height=val+2;
			
			ref_grid = button_skin.hitTestState as ORMSkinGrid;
			ref_grid.height=val;
			
			label=button_label.text;
		}
		
		public override function set label(str:String):void
		{
			super.label=str;
			if (button_label)
			{
				button_label.text=str;
				button_label.x=(button_skin.width-button_label.width)*.5
				button_label.y=(button_skin.height-button_label.height)*.5
			}
		}
		
		public override function draw():void
		{
			super.draw();
			this.visible=true;
		}
	}
}