package ru.saveidea.base {
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;

	/**
	 * @author antonsidorenko
	 */
	public class Wrapper extends Document {
		
		protected var applicationLoader : Loader;
		
		public function Wrapper(applicationHref : String=null) {
			super();
			
			applicationLoader = new Loader();
			applicationLoader.addEventListener(Event.COMPLETE, onApplicationLoadCompleteHandler);
			applicationLoader.addEventListener(ProgressEvent.PROGRESS, onApplicationLoadProgressHandler);
			applicationLoader.load(new URLRequest(applicationHref || root.loaderInfo.parameters['application']));
		}

		protected function onApplicationLoadProgressHandler(event : ProgressEvent) : void {
			
		}

		protected function onApplicationLoadCompleteHandler(event : Event) : void {
			applicationLoader.removeEventListener(Event.COMPLETE, onApplicationLoadCompleteHandler);
			applicationLoader.removeEventListener(ProgressEvent.PROGRESS, onApplicationLoadProgressHandler);
		}
	}
}
