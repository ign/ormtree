package ru.saveidea.base {
	import flash.display.MovieClip;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;

	/**
	 * @author ign
	 */
	public class Document extends MovieClip {

		private var documentResizeTimer : uint;

		public function Document() {
			if (stage) {
				stage.frameRate = 31;
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.align = 'LT';
				stage.addEventListener(Event.RESIZE, initialize);
				documentResizeTimer = setTimeout(initialize, 400);
			}
		}

		protected function initialize(event : Event = null) : void {
			event;
			clearTimeout(documentResizeTimer);
			if (stage) {
				stage.removeEventListener(Event.RESIZE, initialize);
			}
		}
	}
}