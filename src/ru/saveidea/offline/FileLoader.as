package ru.saveidea.offline {
	import flash.system.LoaderContext;
	import ru.saveidea.utils.XMLUtils;

	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.text.StyleSheet;
	import flash.utils.ByteArray;

	/**
	 * @author ign
	 */
	public class FileLoader {

		private static var files : Array = [];

		public static function loadText(href : String, onComplete : Function) : void {
			var urlloader : URLLoader = new URLLoader();
			urlloader.addEventListener(Event.COMPLETE, onTextLoadedHandler);
			urlloader.load(new URLRequest(href));
			
			var file : Object = {href:href, onComplete:onComplete, loader : urlloader};
			files.push(file);
		}

		public static function loadXML(href : String,onComplete : Function) : void {
			
			var urlloader : URLLoader = new URLLoader();
			urlloader.addEventListener(Event.COMPLETE, onXMLLoadedHandler);
			urlloader.load(new URLRequest(href));
			
			var file : Object = {href:href, onComplete:onComplete, loader : urlloader};
			files.push(file);
		}
		
		public static function loadFlash(href : String, onComplete : Function) : void {
			var loader : Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onFlashLoadedHandler);
			loader.load(new URLRequest(href));			
			var file : Object = {href:href, onComplete:onComplete, loader : loader.contentLoaderInfo};
			files.push(file);
		}

		public static function loadBytes(href : String,onComplete : Function) : void {
			var urlloader : URLLoader = new URLLoader();
			urlloader.dataFormat = URLLoaderDataFormat.BINARY;
			urlloader.addEventListener(Event.COMPLETE, onBytesLoadedHandler);
			urlloader.load(new URLRequest(href));
			
			var file : Object = {href:href, onComplete:onComplete, loader : urlloader};
			files.push(file);
		}

		private static function onBytesLoadedHandler(event : Event) : void {			
			var target : URLLoader = event.target as URLLoader;
			target.removeEventListener(Event.COMPLETE, onXMLLoadedHandler);
			var file : Object = removeFileWithLoader(target);
			file['onComplete']((file['loader'] as URLLoader).data);
		}

		
		public static function loadBitmap(href : String,onComplete : Function,context : LoaderContext) : void {
			var loader : Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onBitmapLoadedHandler);
			loader.load(new URLRequest(href),context);
			
			var file : Object = {href:href, onComplete:onComplete, loader:loader.contentLoaderInfo};
			files.push(file);
		}

		public static function loadStyleSheet(href : String,onComplete : Function) : void {
			var urlloader : URLLoader = new URLLoader();
			urlloader.addEventListener(Event.COMPLETE, onCSSLoadedHandler);
			urlloader.load(new URLRequest(href));
			
			var file : Object = {href:href, onComplete:onComplete, loader : urlloader};
			files.push(file);
		}

		private static function onCSSLoadedHandler(event : Event) : void {
			var target : URLLoader = event.target as URLLoader;
			target.removeEventListener(Event.COMPLETE, onXMLLoadedHandler);
			var file : Object = removeFileWithLoader(target);
			var stylesheet : StyleSheet = new StyleSheet();
			stylesheet.parseCSS(target.data);
			file['onComplete'](stylesheet);
		}

		private static function onTextLoadedHandler(event : Event) : void {
			var target : URLLoader = event.target as URLLoader;
			target.removeEventListener(Event.COMPLETE, onXMLLoadedHandler);
			var file : Object = removeFileWithLoader(target);
			file['onComplete']((file['loader'] as URLLoader).data);
		}

		private static function onBitmapLoadedHandler(event : Event) : void {
			var target : LoaderInfo = event.target as LoaderInfo;
			target.addEventListener(Event.COMPLETE, onBitmapLoadedHandler);
			var file : Object = removeFileWithLoader(target);
			file['onComplete'](target.content as Bitmap);
		}
		
		private static function onXMLLoadedHandler(event : Event) : void {
			var target : URLLoader = event.target as URLLoader;
			target.removeEventListener(Event.COMPLETE, onXMLLoadedHandler);
			var file : Object = removeFileWithLoader(target);
			file['onComplete'](XMLUtils.makeXMLSafety((file['loader'] as URLLoader).data));
		}
		
		private static function onFlashLoadedHandler(event : Event) : void {
			var target : LoaderInfo  = event.target as LoaderInfo;
			target.removeEventListener(Event.COMPLETE, onFlashLoadedHandler);
			var file : Object = removeFileWithLoader(target);
			file['onComplete'](target.content);
		}

		private static function removeFileWithLoader(loader : Object) : Object {
			var file : Object;
			for (var i : int = 0;i < files.length;i++) {
				file = files[i];
				if (file['loader'] == loader) {
					files.splice(i, 1);
					break;
				}
			}
			return file;
		}
	}
}
