package ru.saveidea.orm.forms {
	import flash.utils.Dictionary;
	import ru.saveidea.orm.DataTypeList;

	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileReference;

	/**
	 * @author OldMan
	 * Добавляет функционал для работы с файлами в моделях
	 * FIXME:Сделать, чтобы работа шла с File, а не с FileReference
	 */
	public class ModelForm extends ModelFormBase {
		
		protected var filesFolder : File;
		private var fileReference2AttributeViewHash : Dictionary;
		
		public function ModelForm(filesFolder : File = null) {
			super();
			
			this.filesFolder = filesFolder;
			if (!this.filesFolder) {
				this.filesFolder = File.applicationStorageDirectory.resolvePath("files");
			}			
			
			fileReference2AttributeViewHash = new Dictionary();
		}

		override protected function updateModelAttributeFromView(accessor : XML) : void {
			// super.updateModelAttributeFromView(accessor);

			var attributeView : AttributeView = attributeByName[accessor.attribute("name")];

			if (!attributeView) {
				return;
			}

			if (attributeView.accessorProperties.dataType == DataTypeList.FILE) {
				var fileAttributeView : FileAttributeView = attributeView.getCustomAttributeView() as FileAttributeView;

				// Если был старый файл, его удаляем, новый добавляем и ссылку на него записываем в поле

				if (fileAttributeView.file) {
					if (fileAttributeView.value) {
						// удаляем файл						

						var file : File = filesFolder.resolvePath(attributeView.value as String);
						if (file.exists) {
							file.deleteFile();
						}
					}
					
					fileReference2AttributeViewHash[fileAttributeView.file]=attributeView;

					fileAttributeView.file.addEventListener(Event.COMPLETE, onFileLoadCompleteHandler);
					fileAttributeView.file.load();
					
					fileAttributeView.value = fileAttributeView.file.name;
					model[accessor.attribute("name")] = fileAttributeView.file.name;
				}
			} else {
				model[accessor.attribute("name")] = attributeView.value;
			}
		}

		private function onFileLoadCompleteHandler(event : Event) : void {
			//trace("event.target", event.target);
			var target : FileReference = event.target as FileReference;
			
			var attributeView : AttributeView = fileReference2AttributeViewHash[target];
			fileReference2AttributeViewHash[target] = null;

			// Файл загружен, кладём его в какую-то директорию
			var file : File;
			file = filesFolder.resolvePath(attributeView.accessorProperties.path || target.name);
			var fileStream : FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeBytes(target.data);
			fileStream.addEventListener(Event.CLOSE, fileClosed);
			fileStream.close();
		}

		private function fileClosed(event : Event) : void {
			trace("File closed");
		}
	}
}
