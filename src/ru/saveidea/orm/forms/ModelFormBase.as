﻿package ru.saveidea.orm.forms {
	import mx.collections.ArrayCollection;
	import ru.saveidea.minimalcomps.components.ORMButton;
	import ru.saveidea.orm.AccessorProperties;
	import ru.saveidea.orm.forms.view.AttributeViewList;
	import ru.saveidea.orm.forms.view.MCAttributeViewList;
	import ru.saveidea.orm.utils.DescribeTypeUtils;
	import ru.saveidea.view.ManualSizedSprite;

	import flash.events.MouseEvent;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;

	/**
	 * @author antonsidorenko
	 * 
	 * Нужен, для того, чтобы использовать его в локальных проектах, для быстрого создания интерфейсов для редактирование полей сущностей (не содержит File)
	 */
	public class ModelFormBase extends ManualSizedSprite {
		private var _model : Object;
		private var modelClass : Class;
		private var modelAccessors : XML;
		private var attributeViews : Array;
		protected var attributeByName : Object;
		private var save : ORMButton;
		private var attributeViewList : AttributeViewList;

		public function ModelFormBase() {
			/*
			 * Получает модель, пробегается по полям, строит форму, 
			 * каждому полю свой AttrbuteView, указывает текущее
			 * значение для attributeView на базе заполненных данных в моделе
			 * 
			 * 
			 * 
			 */

			attributeViewList = makeAttributeViewList();
			addChild(attributeViewList);

			attributeViews = [];
			attributeByName = {};

			// mainPanel = new Panel();
			// mainPanel.width = 500;
			// mainPanel.height = 500;
			// addChild(mainPanel);

			// attributesPanel = new ScrollPane();
			// attributesPanel.autoHideScrollBar = true;

			// attributesPanel.x = 10;
			// attributesPanel.y = 30;
			// attributesPanel.width = mainPanel.width - attributesPanel.x * 2;
			// attributesPanel.height = mainPanel.height - attributesPanel.y - 40;
			// addChild(attributesPanel);

			save = new ORMButton();
			save.addEventListener(MouseEvent.CLICK, onSaveClickHandler);
			save.label = "Save";
			// save.x = mainPanel.width - save.width - 10;
			// save.y = mainPanel.height - save.height - 10;
			addChild(save);
		}

		protected function makeAttributeViewList() : AttributeViewList {
			return new MCAttributeViewList();
		}

		private function onSaveClickHandler(event : MouseEvent) : void {
			trace("ModelFormBase.onSaveClickHandler(event)");

			// сохранить модель
			// еще раз пробегаю по всем полям и забираю данные из просмотровщиков аттрибутов

			var accessors : XMLList = modelAccessors.descendants("accessor");
			var accessor : XML;
			for (var i : int = 0; i < accessors.length(); i++) {
				accessor = accessors[i];
				if (accessor.attribute("name") != "prototype" && accessor.attribute("name") != "id") {
					updateModelAttributeFromView(accessor);
				}
			}

			dispatchEvent(new ModelFormBaseEvent(ModelFormBaseEvent.SAVE));
		}

		protected function updateModelAttributeFromView(accessor : XML) : void {
			var attributeView : AttributeView = attributeByName[accessor.attribute("name")];

			if (!attributeView) {
				return;
			}

			model[accessor.attribute("name")] = attributeView.value;
		}

		public function clear() : void {
			var attributeView : AttributeView;
			for (var i : int = 0; i < attributeViews.length; i++) {
				attributeView = attributeViews[i];
				attributeView.removeEventListener(AttributeViewEvent.ON_CHANGE, onAttributeChange);
				attributeView.removeEventListener(AttributeViewEvent.MAKE_CHILD, onMakeChild);
			}
			attributeByName = {};
			attributeViews = [];
			attributeViewList.clear();
		}

		public function set model(model : Object) : void {
			clear();

			_model = model;

			modelClass = getDefinitionByName(getQualifiedClassName(model)) as Class;

			modelAccessors = DescribeTypeUtils.getOrderedAccessorsByArgumentInMetaDataList(modelClass, ["Mapped", "ManyToOne", "OneToMany"], "orderIndex");

			var accessors : XMLList = modelAccessors.descendants("accessor");

			var accessor : XML;
			for (var i : int = 0; i < accessors.length(); i++) {
				accessor = accessors[i];

				if (isEditableAccessor(accessor)) {
					addAttributeField(accessor);
				}
			}
		}

		private function isEditableAccessor(accessor : XML) : Boolean {
			var isEditable : Boolean = true;

			var accessorProperties : AccessorProperties = new AccessorProperties();
			accessorProperties.parse(accessor);

			if (accessorProperties.isHidden || accessor.attribute("name") == "prototype") {
				return false;
			}
			if (accessorProperties.containMetaData("Id")) {
				return false;
			}
			if (accessorProperties.containMetaData("Transient")) {
				return false;
			}

			return isEditable;
		}

		protected function addAttributeField(accessor : XML) : void {
			var accessorProperties : AccessorProperties = new AccessorProperties();
			accessorProperties.parse(accessor);

			var attributeField : AttributeView = makeAttributeView(accessorProperties);
			attributeField.value = _model[accessorProperties.name];

			attributeField.addEventListener(AttributeViewEvent.ON_CHANGE, onAttributeChange);
			attributeField.addEventListener(AttributeViewEvent.MAKE_CHILD, onMakeChild);

			attributeViewList.addAttributeView(attributeField);

			attributeViews.push(attributeField);

			tryToProvideData(attributeField);

			attributeByName[accessorProperties.name] = attributeField;
		}

		protected function makeAttributeView(accessorProperties : AccessorProperties) : AttributeView {
			return new AttributeView(accessorProperties);
		}

		private function onMakeChild(event : AttributeViewEvent) : void {
			var childModelForm : ModelFormBase = new ModelFormBase();

			if (event.parentPropertyName) {
				event.childModel[event.parentPropertyName] = model;
			}

			childModelForm.model = event.childModel;

			addChild(childModelForm);
		}

		private function tryToProvideData(attributeView : AttributeView) : void {
			if (attributeView.accessorProperties.dataProvider) {
				// нужно обновить
				
				var dataProvidersLinks : Array = [];
				var dataProvidersList : Array = attributeView.accessorProperties.dataProvider.split(",");
				var dataProviderPath : String;

				for (var i : int = 0; i < dataProvidersList.length; i++) {
					dataProviderPath = dataProvidersList[i];

					var dataProviderArray : Array = dataProviderPath.split(".");
					// var hasAccessError : Boolean = false;
					try {
						var object : Object = model;
						for (var ii : int = 0; ii < dataProviderArray.length; ii++) {
							if (object == model && attributeByName[dataProviderArray[ii]] != null) {
								object = (attributeByName[dataProviderArray[ii]] as AttributeView).value;
							} else {
								object = object[dataProviderArray[ii]];
							}
						}
					} catch (e : Error) {
						// данные недоступны
						// hasAccessError = true;
						// throw new Error(e);
					}
					if (object) {
						dataProvidersLinks.push(object);
					}
				}

				if (dataProvidersLinks.length>0) {
					
					var collectedData : ArrayCollection = new ArrayCollection();
					
					//собираем все данные в один массив и его передаём
					var dataProvider : ArrayCollection;
					for (var iii : int = 0; iii < dataProvidersLinks.length; iii++) {
						dataProvider = dataProvidersLinks[iii];
						collectedData.source = collectedData.source.concat(dataProvider.source);
					}
					
					
					trace("provide data type:", getDefinitionByName(getQualifiedClassName(object)));
					attributeView.data = collectedData;
				}
			}
		}

		private function onAttributeChange(event : AttributeViewEvent) : void {
			var target : AttributeView = event.currentTarget as AttributeView;

			var attributeView : AttributeView;
			for (var i : int = 0; i < attributeViews.length; i++) {
				attributeView = attributeViews[i];
				if (attributeView.accessorProperties.dataProvider && attributeView.accessorProperties.dataProvider.split(".")[0] == target.accessorProperties.name) {
					// нужно обновить

					tryToProvideData(attributeView);
				}
			}
		}

		public function get model() : Object {
			return _model;
		}

		override public function set width(value : Number) : void {
			super.width = value;
			attributeViewList.width = value;
			save.x = value - save.width - 10;
		}

		override public function set height(value : Number) : void {
			super.height = value;

			save.y = value - save.height - 15;

			attributeViewList.height = value - save.height - 20;
		}
	}
}