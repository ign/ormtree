package ru.saveidea.orm.forms {
	import avmplus.getQualifiedClassName;
	
	import com.bit101.components.ComboBox;
	
	import flash.events.Event;
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	
	import ru.saveidea.minimalcomps.components.skins.ORMComboBoxSkin;
	import ru.saveidea.orm.AccessorProperties;
	import ru.saveidea.orm.DataTypeList;
	

	/**
	 * @author antonsidorenko
	 */
	public class ManyToOneAttributeView extends MinimalCompsAttributeView {
		protected var combo : ComboBox;

		public function ManyToOneAttributeView(accessorProperties : AccessorProperties) {
			super(accessorProperties);

			trace("ManyToOneAttributeView", "content data type:", accessorProperties.contentDataType);

			combo = new ComboBox();
			combo.skinClass=ORMComboBoxSkin;
			combo.addEventListener(Event.SELECT, onSelectHandler);
			
			//FIXME:харткод что бы комбобокс не дергался
			combo.height=30;
			
			addChild(combo);
			expandInputWide(combo);
		}

		protected function onSelectHandler(event : Event) : void {
			if (accessorProperties.originalDataType == DataTypeList.STRING) {
				value = String(combo.selectedItem["data"]);
			} else {
				value = combo.selectedItem["data"];
			}
		}

		private function clear() : void {
			combo.removeEventListener(Event.SELECT, onSelectHandler);
			combo.selectedIndex = -1;
			combo.addEventListener(Event.SELECT, onSelectHandler);

			combo.removeAll();
		}

		override public function set data(value : Object) : void {
			clear();
			
			var item : Object;

			if (value is Array || getDefinitionByName("mx.collections::ArrayCollection") == getDefinitionByName(getQualifiedClassName(value))) {
				for (var i : int = 0; i < value["length"]; i++) {
					item = value[i];
					combo.addItem({label:String(item), data:item});
					if (item==this.value) {
						combo.selectedIndex = combo.items.length-1;
					}
				}
			} else {
				throw new Error("Unknown data:" + value + " " + describeType(value));
			}
		}

		override public function set value(value : Object) : void {
			super.value = value;

			if (!value) {
				return;
			}

			if (combo.items && combo.items.length == 0) {
				combo.addItem({data:value, label:String(value)});
			}
			
			onChange(null);
			
			/*
			var item : Object;

			var selectedIndex : Number;
			
			for (var i : int = 0; i < combo.items.length; i++) {
				item = combo.items[i];

				if (value is String) {
					if (String(item["data"]) == value) {
						selectedIndex = i;
						break;
					}
				} else {
					trace("OMG", item, item["data"]);
					if (item["data"]["id"] == value["id"]) {
						selectedIndex = i;
						break;
					}
				}
			}

			if (!isNaN(selectedIndex)) {
				combo.removeEventListener(Event.SELECT, onSelectHandler);
				combo.selectedIndex = selectedIndex;
				combo.addEventListener(Event.SELECT, onSelectHandler);
				onChange(null);
			}
			 * 
			 */
		}
	}
}