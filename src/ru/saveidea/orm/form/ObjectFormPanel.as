package ru.saveidea.orm.form {
	import com.bit101.components.Panel;
	
	import mx.states.OverrideBase;
	
	import ru.saveidea.minimalcomps.components.skins.ORMPanelSkin;
	import ru.saveidea.orm.forms.ModelFormBase;

	/**
	 * @author antonsidorenko
	 */
	public class ObjectFormPanel extends Panel {
		
		private var _data : Object;
		public var form : ObjectForm;

		public function ObjectFormPanel(formInstance : ModelFormBase) {
			form = new ObjectForm(formInstance);
			form.width = 800;
			form.height = 500;
			
			this.skinClass=ORMPanelSkin;

			form.addEventListener(ObjectFormEvent.SAVE, onModelSaveHandler);
			addChild(form);
		}

		private function onModelSaveHandler(event : ObjectFormEvent) : void {
			dispatchEvent(new ObjectFormPanelEvent(ObjectFormPanelEvent.SAVE, _data));
		}

		public function set data(data : Object) : void {
			_data = data;
			form.data = data;
		}

		override public function set width(w : Number) : void {
			super.width = w;
			form.width = w;
		}

		override public function set height(h : Number) : void {
			super.height = h;
			form.height = h;
		}
		override public function draw():void
		{
			super.draw();
		}
	}
}